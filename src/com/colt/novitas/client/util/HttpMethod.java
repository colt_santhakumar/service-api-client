/**
 * 
 */
package com.colt.novitas.client.util;

/**
 * @author omerio
 *
 */
public enum HttpMethod {
    
    GET, PUT, DELETE, POST;

}
