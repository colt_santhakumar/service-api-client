/**
 * 
 */
package com.colt.novitas.client.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.slf4j.MDC;


/**
 * Generic http utility for making API client calls
 * @author omerio
 *
 */
public class HttpUtils {
    
    
    /**
     * Make a get request to the provided url and the relevant callback methods will be called
     * @param url
     * @param callback
     */
    public static void connect(String url, String username, String password, Callback callback) {
        connect(url, username, password, null, null, callback);
    }
    
    /**
     * A generic connect method, instead of copy-paste the code in multiple methods
     * @param url
     * @param httpMethod
     * @param body
     * @param callback
     */
    public static void connect(String url, String username, String password, HttpMethod httpMethod, String body, Callback callback) {
        connect(url, username, password, httpMethod, body, callback, (Integer []) null);
    }
    
    /**
     * A generic connect method, instead of copy-paste the code in multiple methods
     * @param url
     * @param httpMethod
     * @param body
     * @param callback
     */
    public static void connect(String url, String username, String password, HttpMethod httpMethod, String body, Callback callback, Integer... successCodes) {

        BufferedReader reader = null;
        
        List<Integer> codes = null;
        
        if(successCodes != null) {
            codes = Arrays.asList(successCodes);
        }
        
        if(httpMethod == null) {
            httpMethod = HttpMethod.GET;
        }

        try {

            HttpClient client = new HttpClient();

            // Send GET request
            HttpMethodBase method = null;
            
            StringRequestEntity requestEntity = null; 
            
            // do we have a body
            if(body != null) {
               requestEntity = new StringRequestEntity(body.toString(), "application/json",  "UTF-8");
               System.out.println("JSON request: " + body);
                
            } 
            
            switch(httpMethod) {
            case DELETE:
                method = new DeleteMethod(url);
                break;
                
            case GET:
                method = new GetMethod(url);
                break;
                
            case POST:
                method = new PostMethod(url);
                ((PostMethod) method).setRequestEntity(requestEntity);
                break;
                
            case PUT:
                method = new PutMethod(url);
                ((PutMethod) method).setRequestEntity(requestEntity);
                break;
            default:
                break;
            
            }
            
            // Basic Auth
            if(username != null && password != null) {
                String authString = username + ":" + password;
                byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
                method.addRequestHeader("Authorization", "Basic " + new String(authEncBytes));
            }

            // Add Headers
            method.addRequestHeader("Content-Type", "application/json");
            method.addRequestHeader("x-NovitasApiId", MDC.get("unique_log_id"));
            
            //LOG.info("\nSending 'GET' request to URL : " + url);

            int responseCode = client.executeMethod(method);
            
            StringBuilder builder = new StringBuilder();
            InputStream input = method.getResponseBodyAsStream();
            
            if(input != null) {
                reader = new BufferedReader(new InputStreamReader(input, Charset.forName("UTF8")));

                //LOG.info("Response Code : " + responseCode);

                String inputLine;


                while ((inputLine = reader.readLine()) != null) {
                    builder.append(inputLine);
                }
            }

            String response = builder.toString();
            
            System.out.println("JSON response: " + response);

            if (responseCode == 200 || (codes != null && codes.contains(responseCode))) {

                callback.success(response, responseCode);

            } else {

                // Print Error result
                System.out.println("Error response returned, code: " + responseCode + ", reponse: " + response);

                callback.failure(response, responseCode);
            }

        } catch (IOException e) {

            //LOG.error("API call failed", e);
            e.printStackTrace();
            
            callback.failure(e);

        } finally {

            try {
                if (reader != null) {
                    reader.close();
                }

            } catch (IOException e) {
                //LOG.error("Stream close failed", e);
                e.printStackTrace();
            }
        }

    }

}
