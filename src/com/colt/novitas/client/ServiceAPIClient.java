package com.colt.novitas.client;

import com.colt.novitas.client.util.Callback;
import com.colt.novitas.client.util.HttpMethod;
import com.colt.novitas.client.util.HttpUtils;
import com.colt.novitas.constants.NovitasServiceStatus;
import com.colt.novitas.request.*;
import com.colt.novitas.response.*;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.MDC;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

/**
 * 
 * @author CoderSlay
 *
 */
public class ServiceAPIClient {
    
	private static final Logger logger = Logger.getLogger(MDC.get("unique_log_id")+"|"+ServiceAPIClient.class.getName());
	
    private static final Gson GSON = new GsonBuilder().registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
		@Override
		public JsonElement serialize(Date src, Type srcType, JsonSerializationContext context) {

			DateFormat iso8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			iso8601Format.setTimeZone(TimeZone.getTimeZone("UTC"));

			return new JsonPrimitive(iso8601Format.format(src));
		}

	}).create();
    
    private String serviceApiBaseUrl = "http://amsnov02:8080/service-inventory/api";
	//private String serviceApiPortUrl = "http://localhost:8080/service-inventory/api/port/";
	//private String serviceApiServiceUrl = "http://localhost:8080/service-inventory/api/service/";
	//private String serviceApiConnectionUrl = "http://localhost:8080/service-inventory/api/connection/";
	private String serviceApiUsername = "NovServInvUser59";
	private String serviceApiPassword = "NdxUCCd2Ff32jkve";

	SimpleDateFormat fdf = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Constructor to assign Port URL, Connection URL, Username & Password
	 * dynamically.
	 * 
	 * serviceApiPortUrl = "http://localhost:8099/service-inventory/api/port/";
	 * serviceApiConnectionUrl =
	 * "http://localhost:8099/service-inventory/api/connection/";
	 * serviceApiUsername = "NovServInvUser59"; serviceApiPassword =
	 * "NdxUCCd2Ff32jkve";
	 * 
	 * @param serviceApiPortUrl
	 * @param serviceApiConnectionUrl
	 * @param serviceApiUsername
	 * @param serviceApiPassword
	 */
	public ServiceAPIClient(String serviceApiBaseUrl, String serviceApiUsername,
			String serviceApiPassword) {
		super();
		this.serviceApiBaseUrl = serviceApiBaseUrl;
		this.serviceApiUsername = serviceApiUsername;
		this.serviceApiPassword = serviceApiPassword;
		
		
	}
	
	public String createPort(CreatePortRequest portRequest) {

		String createdPortId = null;

		String url = serviceApiBaseUrl+"/port/";

		HttpClient client = null;
		PostMethod postMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {

			client = new HttpClient();

			String authString = serviceApiUsername + ":" + serviceApiPassword;
			byte[] authEncBytes = Base64.encodeBase64((byte[]) authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			// Set JSON request
			JSONObject jsonParam = new JSONObject();
			logger.info("SERVICEAPICLIENT ---------------------------->: " + portRequest.getName());
			jsonParam.put("name", portRequest.getName());
			jsonParam.put("location", portRequest.getLocation());
			jsonParam.put("bandwidth", portRequest.getBandwidth());
			jsonParam.put("connector", portRequest.getConnector());
			jsonParam.put("technology", portRequest.getTechnology());
			jsonParam.put("presentation_label", portRequest.getPresentationLabel());
			jsonParam.put("rental_charge", portRequest.getRentalCharge());
			jsonParam.put("rental_unit", portRequest.getRentalUnit());
			jsonParam.put("rental_currency", portRequest.getRentalCurrency());
			jsonParam.put("address", portRequest.getAddress());
			jsonParam.put("site_type", portRequest.getSiteType());
			jsonParam.put("location_building_name",portRequest.getLocationBuildingName());
			jsonParam.put("location_street_name", portRequest.getLocationStreetName());
			jsonParam.put("location_city", portRequest.getLocationCity());
			jsonParam.put("location_country", portRequest.getLocationCountry());
			jsonParam.put("latitude", portRequest.getLatitude());
			jsonParam.put("longitude", portRequest.getLongitude());
			jsonParam.put("site_floor", portRequest.getSiteFloor());
			jsonParam.put("site_room_name", portRequest.getSiteRoomName());
			jsonParam.put("location_state", portRequest.getLocationState());
			jsonParam.put("location_premises_number", portRequest.getLocationPremisesNumber());
			jsonParam.put("postal_zip_code", portRequest.getPostalZipCode());
			jsonParam.put("commitment_period", portRequest.getCommitmentPeriod());
			
			if (portRequest.getCommitmentExpiry() != null) {
				String formattedDate = fdf.format(portRequest.getCommitmentExpiry());
				jsonParam.put("commitment_expiry", formattedDate);
			} else {
				jsonParam.put("commitment_expiry", "");
			}
			jsonParam.put("service_id", portRequest.getServiceId());
			jsonParam.put("resource_id", portRequest.getResourceId());
			jsonParam.put("decommissioning_charge", portRequest.getDecommissioningCharge());
			jsonParam.put("decommissioning_currency", portRequest.getDecommissioningCurrency());
			jsonParam.put("customer_name", portRequest.getCustomerName());
			jsonParam.put("ocn", portRequest.getOcn());
			jsonParam.put("location_id", portRequest.getLocationId());
			jsonParam.put("port_type",portRequest.getPortType());
			jsonParam.put("expiration_period", portRequest.getPortExpiryPeriod());
			jsonParam.put("expires_on", portRequest.getPortExpiresOn());
			jsonParam.put("in_use", portRequest.getIsPortInUse());
			jsonParam.put("loa_allowed", portRequest.isLoaAllowed());
			jsonParam.put("local_building_name",portRequest.getLocalBuildingName());
			jsonParam.put("location_city_code",portRequest.getLocationCityCode());
			jsonParam.put("location_country_code",portRequest.getLocationCountryCode());
			jsonParam.put("customer_port_id", portRequest.getCustomerPortID());
			jsonParam.put("resource_port_name",portRequest.getResourcePortName());
			jsonParam.put("cross_connect_allowed", portRequest.getIsCrossConnectOrderable());
			jsonParam.put("cross_connect_request_id", portRequest.getCrossConnectRequestId());
			jsonParam.put("max_allowed_ipa_conn_bandwidth", portRequest.getMaxAllowedIpaConnBandwidth());
			// Send post request
			StringRequestEntity requestEntity = new StringRequestEntity(jsonParam.toString(), "application/json","UTF-8");
			postMethod = new PostMethod(url);
			postMethod.setRequestEntity(requestEntity);

			// Add Headers
			postMethod.addRequestHeader("Content-Type", "application/json;charset=UTF-8");
			postMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);
			postMethod.addRequestHeader("x-NovitasApiId", MDC.get("unique_log_id"));
			logger.info("POST parameters : " + jsonParam);
			int responseCode = client.executeMethod(postMethod);

			//logger.info("\nSending 'POST' request to URL : " + url);
			
			logger.info("Response Code : " + responseCode);

			if (responseCode == 200 || responseCode == 201) {
				correctResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer responseString = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					responseString.append(inputLine);
				}
				correctResponseReader.close();

				// Print result
				logger.info("Output JSON ------->" + responseString.toString());

				JSONObject responseJson = new JSONObject(responseString.toString());

				createdPortId = responseJson.get("id").toString();

			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer responseString = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					responseString.append(inputLine);
				}
				errorResponseReader.close();

				// Print Error result
				logger.info(responseString.toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}

				if (errorResponseReader != null) {
					errorResponseReader.close();
				}

			} catch (IOException e) {
				e.printStackTrace();

			}
		}

		return createdPortId;
	}

	public Object updatePort(String portId, UpdatePortRequest portReq) {

		String url = serviceApiBaseUrl+"/port/" + portId;

		HttpClient client = null;
		PutMethod putMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {

			client = new HttpClient();

			String authString = serviceApiUsername + ":" + serviceApiPassword;
			byte[] authEncBytes = Base64.encodeBase64((byte[]) authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			// Set JSON request
			JSONObject jsonParam = new JSONObject();
			jsonParam.put("status", portReq.getStatus());
			jsonParam.put("resource_id", portReq.getResourceId());
			jsonParam.put("presentation_label", portReq.getPresentationLabel());
			jsonParam.put("expires_on", portReq.getPortExpiresOn());
			jsonParam.put("in_use", portReq.getPortInUse());
			jsonParam.put("nms", portReq.getNms());
			jsonParam.put("nc_tech_service_id", portReq.getNcTechServiceId());
			jsonParam.put("resource_port_name", portReq.getResourcePortName());
			jsonParam.put("cross_connect_request_id", portReq.getCrossConnectRequestId());
			jsonParam.put("cross_connect_id", portReq.getCrossConnectId());
			jsonParam.put("max_allowed_ipa_conn_bandwidth", portReq.getMaxAllowedIpaConnBandwidth());
			if (portReq.getCommitmentExpiryDate() != null) {
				jsonParam.put("commitment_expiry", portReq.getCommitmentExpiryDate());
			} else {
				jsonParam.put("commitment_expiry", "");
			}
			if (null != portReq.getCommitmentPeriod()) {
				jsonParam.put("commitment_period", portReq.getCommitmentPeriod());
			}
			
			// Send post request
			StringRequestEntity requestEntity = new StringRequestEntity(jsonParam.toString(), "application/json",
					"UTF-8");

			putMethod = new PutMethod(url);
			putMethod.setRequestEntity(requestEntity);

			// Add Headers
			putMethod.addRequestHeader("Content-Type", "application/json");
			putMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);
			putMethod.addRequestHeader("x-NovitasApiId", MDC.get("unique_log_id"));
			int responseCode = client.executeMethod(putMethod);

			logger.info("\nSending 'PUT' request to URL : " + url);
			logger.info("PUT parameters : " + jsonParam);
			logger.info("Response Code : " + responseCode);

			if (responseCode == 200) {
				correctResponseReader = new BufferedReader(new InputStreamReader(putMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				correctResponseReader.close();

				// Print result
				logger.info(response.toString());
			} else if (responseCode == 204) {
				logger.info("Successfully updated the port");
				return "SUCCESS";
			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(putMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();

				// Print Error result
				logger.info(response.toString());
				return "FAILED";
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}

				if (errorResponseReader != null) {
					errorResponseReader.close();
				}

			} catch (IOException e) {
				e.printStackTrace();

			}
		}

		return new Object();
	}

	public String createConnection(ConnectionRequestService connReq) {

		String createdConnectionId = null;

		String url = serviceApiBaseUrl+"/connection/";

		HttpClient client = null;
		PostMethod postMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {
			client = new HttpClient();

			String authString = serviceApiUsername + ":" + serviceApiPassword;
			byte[] authEncBytes = Base64.encodeBase64((byte[]) authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			// Set JSON request
			JSONObject jsonParam = new JSONObject();
			jsonParam.put("name", connReq.getName());
			jsonParam.put("from_port_id", connReq.getFromPortId());
			jsonParam.put("to_port_id", connReq.getToPortId());
			jsonParam.put("resource_id", connReq.getResourceId());
			jsonParam.put("from_uni_type", connReq.getFromUniType());
			jsonParam.put("to_uni_type", connReq.getToUniType());
			jsonParam.put("bandwidth", connReq.getBandwidth());
			jsonParam.put("rental_charge", connReq.getRentalCharge());
			jsonParam.put("rental_unit", connReq.getRentalUnit());
			jsonParam.put("rental_currency", connReq.getRentalCurrency());
			jsonParam.put("connection_type", connReq.getConnectionType());
			jsonParam.put("service_id", connReq.getServiceId());
			jsonParam.put("a_end_vlan_mapping", connReq.getFromVlanMapping());
			jsonParam.put("b_end_vlan_mapping", connReq.getToVlanMapping());
			jsonParam.put("a_end_vlan_type", connReq.getFromVlanType());
			jsonParam.put("b_end_vlan_type", connReq.getToVlanType());
			jsonParam.put("customer_name", connReq.getCustomerName());
			jsonParam.put("ocn", connReq.getOcn());
			jsonParam.put("commitment_period", connReq.getCommitmentPeriod());
			jsonParam.put("commitment_expiry_date", connReq.getCommitmentExpiryDate());
			jsonParam.put("connection_type", connReq.getConnectionType());
			jsonParam.put("nms", connReq.getNms());
			final List<JSONObject> fromRanges = createVLANRanges(connReq.getFromPortVLANIdRange());
			if (!fromRanges.isEmpty()) {
				jsonParam.put("a_end_vlan_ids", new JSONArray(fromRanges));
			}

			final List<JSONObject> toRanges = createVLANRanges(connReq.getToPortVLANIdRange());
			if (!toRanges.isEmpty()) {
				jsonParam.put("b_end_vlan_ids", new JSONArray(toRanges));
			}

			// Send post request
			StringRequestEntity requestEntity = new StringRequestEntity(jsonParam.toString(), "application/json",
					"UTF-8");

			postMethod = new PostMethod(url);
			postMethod.setRequestEntity(requestEntity);

			// Add Headers
			postMethod.addRequestHeader("Content-Type", "application/json");
			postMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);
			postMethod.addRequestHeader("x-NovitasApiId", MDC.get("unique_log_id"));
			int responseCode = client.executeMethod(postMethod);
			logger.info("\nSending 'POST' request to URL : " + url);
			logger.info("POST parameters : " + jsonParam);
			logger.info("Response Code : " + responseCode);

			if (responseCode == 200 || responseCode == 201) {
				correctResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				correctResponseReader.close();

				// Print result
				logger.info("Connection JSON response ------>" + response.toString());

				JSONObject responseJson = new JSONObject(response.toString());

				createdConnectionId = responseJson.get("id").toString();

			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();

				// Print Error result
				logger.info(response.toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}

				if (errorResponseReader != null) {
					errorResponseReader.close();
				}

			} catch (IOException e) {
				e.printStackTrace();

			}
		}

		return createdConnectionId;
	}

	public Object updateConnection(String connectionId, UpdateConnRequest connReq) {

		String url = serviceApiBaseUrl+"/connection/" + connectionId;

		HttpClient client = null;
		PutMethod putMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {
			client = new HttpClient();

			String authString = serviceApiUsername + ":" + serviceApiPassword;
			byte[] authEncBytes = Base64.encodeBase64((byte[]) authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			// Set JSON request
			JSONObject jsonParam = new JSONObject();
			jsonParam.put("status", connReq.getStatus());
			jsonParam.put("resource_id", connReq.getResourceId());
			jsonParam.put("bandwidth", connReq.getBandwidth());
			jsonParam.put("rental_charge", connReq.getRentalCharge());
			jsonParam.put("rental_currency", connReq.getRentalCurrency());
			jsonParam.put("rental_unit", connReq.getRentalUnit());
			jsonParam.put("a_end_vlan_mapping", connReq.getFromVlanMapping());
			jsonParam.put("b_end_vlan_mapping", connReq.getToVlanMapping());
			jsonParam.put("a_end_vlan_type", connReq.getFromVlanType());
			jsonParam.put("b_end_vlan_type", connReq.getToVlanType());
			jsonParam.put("commitment_period", connReq.getCommitmentPeriod());
			jsonParam.put("commitment_expiry_date", connReq.getCommitmentExpiryDate());
			jsonParam.put("nc_tech_service_id", connReq.getNcTechServiceId());
			jsonParam.put("olo_service_id", connReq.getOloServiceId());
			if(null != connReq.getBaseBandwidth())
			jsonParam.put("base_bandwidth", connReq.getBaseBandwidth());
			if(null != connReq.getBaseRental())
			jsonParam.put("base_rental", connReq.getBaseRental());

			final List<JSONObject> fromRanges = createVLANRanges(connReq.getFromPortVLANIdRange());
			if (!fromRanges.isEmpty()) {
				jsonParam.put("a_end_vlan_ids", new JSONArray(fromRanges));
			}

			final List<JSONObject> toRanges = createVLANRanges(connReq.getToPortVLANIdRange());
			if (!toRanges.isEmpty()) {
				jsonParam.put("b_end_vlan_ids", new JSONArray(toRanges));
			}

			// Send PUT request
			StringRequestEntity requestEntity = new StringRequestEntity(jsonParam.toString(), "application/json",
					"UTF-8");

			putMethod = new PutMethod(url);
			putMethod.setRequestEntity(requestEntity);

			// Add Headers
			putMethod.addRequestHeader("Content-Type", "application/json");
			putMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);
			putMethod.addRequestHeader("x-NovitasApiId", MDC.get("unique_log_id"));
			int responseCode = client.executeMethod(putMethod);

			logger.info("\nSending 'PUT' request to URL : " + url);
			logger.info("PUT parameters : " + jsonParam);
			logger.info("Response Code : " + responseCode);

			if (responseCode == 200) {
				correctResponseReader = new BufferedReader(new InputStreamReader(putMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				correctResponseReader.close();

				// Print result
				logger.info(response.toString());
			} else if (responseCode == 204) {
				logger.info("Successfully updated connection");
				return "SUCCESS";
			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(putMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();

				// Print Error result
				logger.info(response.toString());
				return "FAILED";
			}

		} catch (Exception e) {

		} finally {

			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}

				if (errorResponseReader != null) {
					errorResponseReader.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return new Object();
	}

	public CustomerDedicatedPortResponse getPort(String portId) {

		String url = serviceApiBaseUrl + "/port/" + portId;

		final List<CustomerDedicatedPortResponse> finalPortResponseList = new ArrayList<CustomerDedicatedPortResponse>();

		CustomerDedicatedPortResponse finalPortResponse = new CustomerDedicatedPortResponse();

		HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, new Callback() {

			@Override
			public void success(String response, int code) {

				CustomerDedicatedPortResponse tmpPortResponse = GSON.fromJson(response,
						new TypeToken<CustomerDedicatedPortResponse>() {
						}.getType());
				finalPortResponseList.add(tmpPortResponse);

			}

			@Override
			public void failure(String response, int code) {
			}

			@Override
			public void failure(Exception e) {
			}

		});

		if (finalPortResponseList.size() > 0) {
			finalPortResponse = finalPortResponseList.get(0);
		}
		return finalPortResponse;
	}
	
	public CustomerDedicatedPortResponse[] getPortsByServiceIdAndDate(String serviceId, String startDate,
			String endDate) {

		String url = serviceApiBaseUrl + "/port/" + "?service_id=" + serviceId + "&start_date=" + startDate
				+ "&end_date=" + endDate;
		
		final List<CustomerDedicatedPortResponse> finalPortResponseList = new ArrayList<CustomerDedicatedPortResponse>();

		HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, new Callback() {

			@Override
			public void success(String response, int code) {

				List<CustomerDedicatedPortResponse> portResponse = GSON.fromJson(response,
						new TypeToken<List<CustomerDedicatedPortResponse>>() {
						}.getType());
				finalPortResponseList.addAll(portResponse);
			}

			@Override
			public void failure(String response, int code) {
			}

			@Override
			public void failure(Exception e) {
			}

		});

		return finalPortResponseList.toArray(new CustomerDedicatedPortResponse[finalPortResponseList.size()]);
	}

	public ConnectionResponse getConnection(String connectionId) {

		String url = serviceApiBaseUrl + "/connection/" + connectionId;

		final List<ConnectionResponse> finalConnectionResponseList = new ArrayList<ConnectionResponse>();

		ConnectionResponse finalConnectionResponse = new ConnectionResponse();

		HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, new Callback() {

			@Override
			public void success(String response, int code) {

				ConnectionResponse tmpPortResponse = GSON.fromJson(response, new TypeToken<ConnectionResponse>() {
				}.getType());
				finalConnectionResponseList.add(tmpPortResponse);
			}

			@Override
			public void failure(String response, int code) {
			}

			@Override
			public void failure(Exception e) {
			}

		});

		if (finalConnectionResponseList.size() > 0) {
			finalConnectionResponse = finalConnectionResponseList.get(0);
		}
		return finalConnectionResponse;
	}
	
	public ConnectionBillResponse[] getConnectionsByServiceIdAndDate(String serviceId, String startDate, String endDate) {

		String url = serviceApiBaseUrl+"/connection/" +"?service_id="+serviceId+"&start_date="+startDate+"&end_date="+endDate;

		final List<ConnectionBillResponse> finalConnectionBillResponseList = new ArrayList<ConnectionBillResponse>();

		HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, new Callback() {

			@Override
			public void success(String response, int code) {

				List<ConnectionBillResponse> connectionBillResponse = GSON.fromJson(response,
						new TypeToken<List<ConnectionBillResponse>>() {
						}.getType());
				finalConnectionBillResponseList.addAll(connectionBillResponse);
			}

			@Override
			public void failure(String response, int code) {
			}

			@Override
			public void failure(Exception e) {
			}

		});
		
		return finalConnectionBillResponseList.toArray(new ConnectionBillResponse[finalConnectionBillResponseList.size()]);
	}
	
	public ServiceResponse[] getServicesByStartAndEndDate(String startDate, String endDate) {

		List<ServiceResponse> finalServiceResponseList = new ArrayList<ServiceResponse>();

		String url = serviceApiBaseUrl+"/service/" +"?start_date="+startDate+"&end_date="+endDate;

		HttpClient client = null;
		GetMethod getMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;
		try {
			client = new HttpClient();

			// Set Credentials
			String authString = serviceApiUsername + ":" + serviceApiPassword;
			byte[] authEncBytes = Base64.encodeBase64((byte[]) authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			// Send GET request
			getMethod = new GetMethod(url);

			// Add Headers
			getMethod.addRequestHeader("Content-Type", "application/json");
			getMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);
			getMethod.addRequestHeader("x-NovitasApiId", MDC.get("unique_log_id"));
			int responseCode = client.executeMethod(getMethod);

			logger.info("\nSending 'GET' request to URL : " + url);
			logger.info("Response Code : " + responseCode);

			if (responseCode == 200) {
				correctResponseReader = new BufferedReader(new InputStreamReader(getMethod.getResponseBodyAsStream()));

				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}

				correctResponseReader.close();

				// Print result
				logger.info(response.toString());
				
				JSONArray jsonArray = new JSONArray(response.toString());
				JSONObject jsonObj = null;
				logger.info("Service Response");
				for(int i = 0; i < jsonArray.length(); i++) {
					jsonObj = jsonArray.getJSONObject(i);
					ServiceResponse serviceResponse = new ServiceResponse();
					
					if (!jsonObj.get("serviceId").toString().equals("null")) {
						serviceResponse.setServiceId(jsonObj.get("serviceId").toString());
					}

					if (!jsonObj.get("customerId").toString().equals("null")) {
						serviceResponse.setCustomerId(Integer.parseInt(jsonObj.get("customerId").toString()));
					}
					
					if (!jsonObj.get("status").toString().equals("null")) {
						serviceResponse.setStatus(NovitasServiceStatus.valueOf(jsonObj.get("status").toString()));
					}
					finalServiceResponseList.add(serviceResponse);
				}

			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(getMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();

				// Print Error result
				logger.info(response.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}

				if (errorResponseReader != null) {
					errorResponseReader.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return finalServiceResponseList.toArray(new ServiceResponse[finalServiceResponseList.size()]);
	}

	public Object updateServiceStatus(String serviceId, String status, String errorStatusDescription) {

		String url = "";

		if (null != errorStatusDescription)
			url = serviceApiBaseUrl+"/service/" + serviceId + "?status=" + status + "&error_description=" + errorStatusDescription;
		else
			url = serviceApiBaseUrl+"/service/" + serviceId + "?status=" + status;

		HttpClient client = null;
		PutMethod putMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {
			client = new HttpClient();

			String authString = serviceApiUsername + ":" + serviceApiPassword;
			byte[] authEncBytes = Base64.encodeBase64((byte[]) authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			putMethod = new PutMethod(url);

			// Add Headers
			//putMethod.addRequestHeader("Content-Type", "application/json");
			putMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);

			int responseCode = client.executeMethod(putMethod);

			logger.info("\nSending 'PUT' request to URL : " + url);
			logger.info("Response Code : " + responseCode);

			if (responseCode == 200) {
				correctResponseReader = new BufferedReader(new InputStreamReader(putMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				correctResponseReader.close();

				// Print result
				logger.info(response.toString());
			} else if (responseCode == 204) {
				logger.info("Successfully updated the service status");
				return "SUCCESS";
			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(putMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();

				// Print Error result
				logger.info(response.toString());
				return "FAILED";
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}
				if (errorResponseReader != null) {
					errorResponseReader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return new Object();
	}
	
	@SuppressWarnings("unused")
	private List<ServiceVLANIdRange> getVLANRanges(final JSONArray ranges) throws JSONException {
		final List<ServiceVLANIdRange> vlanRange = new ArrayList<ServiceVLANIdRange>();
		if (ranges != null && ranges.length() > 0) {
			for (int i = 0; i < ranges.length(); i++) {
				final JSONObject rangeObject = ranges.getJSONObject(i);

				Integer fromIdRange = null;
				try {
					fromIdRange = rangeObject.getInt("from_id_range");
				} catch (final Exception e1) {
				}
				Integer toIdRange = null;
				try {
					toIdRange = rangeObject.getInt("to_id_range");
				} catch (final Exception e) {
				}
				vlanRange.add(new ServiceVLANIdRange(fromIdRange, toIdRange));
			}
		}
		return vlanRange;
	}

	private List<JSONObject> createVLANRanges(List<ServiceVLANIdRange> ranges) throws JSONException {

		final List<JSONObject> rangesJSON = new ArrayList<JSONObject>();

		if (ranges != null && !ranges.isEmpty()) {

			for (final ServiceVLANIdRange range : ranges) {
				final JSONObject rangeObject = new JSONObject();
				rangeObject.put("from_id_range", range.getFromIdRange());
				rangeObject.put("to_id_range", range.getToIdRange());
				rangesJSON.add(rangeObject);
			}

		}
		return rangesJSON;
	}
	
	
    public List<CustomerDedicatedPortResponse> getPorts(String customerId) {

        final List<CustomerDedicatedPortResponse> results = new ArrayList<CustomerDedicatedPortResponse>();
        String url = serviceApiBaseUrl+"/port/" + "?customerid=" + customerId + "&all";

        HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, new Callback() {

            @Override
            public void success(String response, int code) {

                List<CustomerDedicatedPortResponse> ports = 
                		GSON.fromJson(response, new TypeToken<List<CustomerDedicatedPortResponse>>(){}.getType());
                results.addAll(ports);
            }

            @Override
            public void failure(String response, int code) { 
            }

            @Override
            public void failure(Exception e) {                
            }

        });

        return results;
    }
	
    public List<CustomerDedicatedPortResponse> getActivePorts(String customerId) {

        final List<CustomerDedicatedPortResponse> results = new ArrayList<CustomerDedicatedPortResponse>();
        String url = serviceApiBaseUrl+"/port/" + "?customerid=" + customerId;

        HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, new Callback() {

            @Override
            public void success(String response, int code) {

                List<CustomerDedicatedPortResponse> ports = 
                		GSON.fromJson(response, new TypeToken<List<CustomerDedicatedPortResponse>>(){}.getType());
                results.addAll(ports);
            }

            @Override
            public void failure(String response, int code) { 
            }

            @Override
            public void failure(Exception e) {                
            }

        });

        return results;
    }
	
    
    public List<ConnectionResponse> getConnections(String customerId) {

        final List<ConnectionResponse> results = new ArrayList<ConnectionResponse>();
        String url = serviceApiBaseUrl+"/connection/" + "?customerid=" + customerId + "&all";

        HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, new Callback() {

            @Override
            public void success(String response, int code) {

                List<ConnectionResponse> conns = 
                		GSON.fromJson(response, new TypeToken<List<ConnectionResponse>>(){}.getType());
                results.addAll(conns);
            }

            @Override
            public void failure(String response, int code) { 
            }

            @Override
            public void failure(Exception e) {                
            }

        });

        return results;
    }
    
    public List<ConnectionResponse> getActiveConnections(String customerId) {

        final List<ConnectionResponse> results = new ArrayList<ConnectionResponse>();
        String url = serviceApiBaseUrl+"/connection/" + "?customerid=" + customerId;

        HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, new Callback() {

            @Override
            public void success(String response, int code) {

                List<ConnectionResponse> conns = 
                		GSON.fromJson(response, new TypeToken<List<ConnectionResponse>>(){}.getType());
                results.addAll(conns);
            }

            @Override
            public void failure(String response, int code) {
            }

            @Override
            public void failure(Exception e) {                
            }

        });

        return results;
    }
	
	// ------------------------------------------------------- Cloud connections and ports -----------------------------------
	
	/**
     * Create cloud port
     * @param portRequest
     * @return
     */
    public String createCloudPort(CreateCloudPortRequest portRequest) {
        
        final StringBuilder createdPortId = new StringBuilder();
        
        String body = GSON.toJson(portRequest);
        logger.info("Creating DCA Port service and URL and request is :: "+serviceApiBaseUrl+"/cloudport/"+" :: "+body);
        HttpUtils.connect(serviceApiBaseUrl+"/cloudport/", serviceApiUsername, serviceApiPassword, HttpMethod.POST, body, new Callback() {
           
        	@Override
            public void success(String response, int code) {
                CreatePortAndConnectionResponse resp = GSON.fromJson(response, CreatePortAndConnectionResponse.class);
                createdPortId.append(resp.getId());
            }

            @Override
            public void failure(String response, int code) {
            }

            @Override
            public void failure(Exception e) {                
            }
            
        }, 201);
        
        return createdPortId.toString();
    }
    
    /**
     * Update a cloud port
     * @param portId
     * @param portReq
     * @return
     */
    public Object updateCloudPort(String portId, UpdateCloudPortRequest portReq) {

        String url = serviceApiBaseUrl+"/cloudport/" + portId;
        
        
        final StringBuilder results = new StringBuilder();
        
        String body = GSON.toJson(portReq);

        HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, HttpMethod.PUT, body, new Callback() {

            @Override
            public void success(String response, int code) {
                
                logger.info(response);
                
                if (code == 204) {
                    logger.info("Successfully updated the port");
                    results.append("SUCCESS");
                }
            }

            @Override
            public void failure(String response, int code) { 
                results.append("FAILED");
            }

            @Override
            public void failure(Exception e) {                
            }
            
        }, 204);
        
        String value = results.toString();
        return value == "" ? new Object() : value;
    }
    
    /**
     * Get a cloud port by id
     * @param portId
     * @return
     */
    public CloudPortResponse getCloudPort(String portId) {
        
        final List<CloudPortResponse> results = new ArrayList<CloudPortResponse>();
        String url = serviceApiBaseUrl+"/cloudport/" + portId;
        
        HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, new Callback() {

            @Override
            public void success(String response, int code) {
                
                CloudPortResponse port = GSON.fromJson(response, CloudPortResponse.class);
                results.add(port);
            }

            @Override
            public void failure(String response, int code) { 
            }

            @Override
            public void failure(Exception e) {                
            }
            
        });
        
        return results.size() > 0 ? results.get(0) : null;
        
    }
    
    /**
     * Get cloud ports by service id and date
     * @param serviceId
     * @param startDate
     * @param endDate
     * @return
     */
    public List<CloudPortResponse> getCloudPortsByServiceIdAndDate(String serviceId, String startDate, String endDate) {

        final List<CloudPortResponse> results = new ArrayList<CloudPortResponse>();
        String url = serviceApiBaseUrl+"/cloudport/" + "?service_id=" + serviceId + "&start_date=" + startDate + "&end_date=" + endDate;

        HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, new Callback() {

            @Override
            public void success(String response, int code) {

                List<CloudPortResponse> ports = GSON.fromJson(response, new TypeToken<List<CloudPortResponse>>(){}.getType());
                results.addAll(ports);
            }

            @Override
            public void failure(String response, int code) { 
            }

            @Override
            public void failure(Exception e) {                
            }

        });

        return results;
    }
    
    // -------------------------------------------------------------- Cloud Connections
    
    public String createCloudConnection(CreateCloudConnRequest connReq) {
        
        
        final StringBuilder createdConnId = new StringBuilder();
        
        String body = GSON.toJson(connReq);

        HttpUtils.connect(serviceApiBaseUrl+"/cloudconnection/", serviceApiUsername, serviceApiPassword, HttpMethod.POST, body, new Callback() {

            @Override
            public void success(String response, int code) {
                CreatePortAndConnectionResponse resp = GSON.fromJson(response, CreatePortAndConnectionResponse.class);
                createdConnId.append(resp.getId());
            }

            @Override
            public void failure(String response, int code) {
            }

            @Override
            public void failure(Exception e) {                
            }
            
        }, 201);
        
        return createdConnId.toString();
       
    }

    public Object updateCloudConnection(String connectionId, UpdateCloudConnRequest connReq) {
        
        String url = serviceApiBaseUrl+"/cloudconnection/" + connectionId;
        
        
        final StringBuilder results = new StringBuilder();
        
        String body = GSON.toJson(connReq);

        HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, HttpMethod.PUT, body, new Callback() {

            @Override
            public void success(String response, int code) {
                
                logger.info(response);
                
                if (code == 204) {
                    logger.info("Successfully updated the port");
                    results.append("SUCCESS");
                }
            }

            @Override
            public void failure(String response, int code) { 
                results.append("FAILED");
            }

            @Override
            public void failure(Exception e) {                
            }
            
        }, 204);
        
        String value = results.toString();
        return value == "" ? new Object() : value;
        
    }
    
    public CloudConnectionResponse getCloudConnection(String connectionId) {
        
        final List<CloudConnectionResponse> results = new ArrayList<CloudConnectionResponse>();
        String url = serviceApiBaseUrl+"/cloudconnection/" + connectionId;
        
        HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, new Callback() {

            @Override
            public void success(String response, int code) {
                
                CloudConnectionResponse conn = GSON.fromJson(response, CloudConnectionResponse.class);
                results.add(conn);
            }

            @Override
            public void failure(String response, int code) { 
            }

            @Override
            public void failure(Exception e) {                
            }
            
        });
        
        return results.size() > 0 ? results.get(0) : null;
        
    }
    
    /**
     * Get cloud ports by service id and date
     * @param serviceId
     * @param startDate
     * @param endDate
     * @return
     */
    public List<CloudConnectionBillResponse> getCloudConnectionsByServiceIdAndDate(String serviceId, String startDate, String endDate) {

        final List<CloudConnectionBillResponse> results = new ArrayList<CloudConnectionBillResponse>();
        String url = serviceApiBaseUrl+"/cloudconnection/" + "?service_id=" + serviceId + "&start_date=" + startDate + "&end_date=" + endDate;

        HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, new Callback() {

            @Override
            public void success(String response, int code) {

                List<CloudConnectionBillResponse> connections = GSON.fromJson(response, new TypeToken<List<CloudConnectionBillResponse>>(){}.getType());
                results.addAll(connections);
            }

            @Override
            public void failure(String response, int code) { 
            }

            @Override
            public void failure(Exception e) {                
            }

        });

        return results;
    }
    
    public List<CloudConnectionResponse> getActiveDCAConnections(String customerId) {

        final List<CloudConnectionResponse> results = new ArrayList<CloudConnectionResponse>();
        String url = serviceApiBaseUrl+"/cloudconnection/" + "?customerid=" + customerId;

        HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, new Callback() {

            @Override
            public void success(String response, int code) {

                List<CloudConnectionResponse> conns = 
                		GSON.fromJson(response, new TypeToken<List<CloudConnectionResponse>>(){}.getType());
                results.addAll(conns);
            }

            @Override
            public void failure(String response, int code) {
            }

            @Override
            public void failure(Exception e) {                
            }

        });

        return results;
    }
    
    public List<CloudPortResponse> getActiveCloudPorts(String customerId) {

        final List<CloudPortResponse> results = new ArrayList<CloudPortResponse>();
        String url = serviceApiBaseUrl+"/cloudport/" + "?customerid=" + customerId;

        HttpUtils.connect(url, serviceApiUsername, serviceApiPassword, new Callback() {

            @Override
            public void success(String response, int code) {

                List<CloudPortResponse> ports = 
                		GSON.fromJson(response, new TypeToken<List<CloudPortResponse>>(){}.getType());
                results.addAll(ports);
            }

            @Override
            public void failure(String response, int code) { 
            }

            @Override
            public void failure(Exception e) {                
            }

        });

        return results;
    }
    
   
   
	
}
