package com.colt.novitas.constants;

public enum ConnectionStatus {
	PENDING, ACTIVE, MODIFYING , DECOMMISSIONING, DECOMMISSIONED;

}
