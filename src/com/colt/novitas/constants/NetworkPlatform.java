package com.colt.novitas.constants;

public enum NetworkPlatform {
    IQNET,
    MMSP
}
