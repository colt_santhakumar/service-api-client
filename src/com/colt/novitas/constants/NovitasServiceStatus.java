package com.colt.novitas.constants;

public enum NovitasServiceStatus {

	ACTIVE, INACTIVE, ARCHIVED;

}
