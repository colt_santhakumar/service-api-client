package com.colt.novitas.constants;

public enum PortStatus {
	PENDING, ACTIVE, ABORTED,DECOMMISSIONING, DECOMMISSIONED;
}
