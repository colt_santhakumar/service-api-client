package com.colt.novitas.constants;

public enum PortType {
	P;
	
	public static PortType validate(String value) {
        try {
            return PortType.valueOf(value);
        } catch (Exception e) {
            return null;
        }
    }
}
