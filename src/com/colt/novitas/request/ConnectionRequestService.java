package com.colt.novitas.request;

import java.io.Serializable;
import java.util.List;

import com.colt.novitas.response.ServiceVLANIdRange;

public class ConnectionRequestService implements Serializable {

	private static final long serialVersionUID = 2137359073739437377L;

	private String name;
	private String fromPortId;
	private String toPortId;
	private String resourceId;
	private String fromUniType;
	private String toUniType;
	private Integer bandwidth;
	private Float rentalCharge;
	private String rentalUnit;
	private String rentalCurrency;
	private String connectionType;
	private String serviceId;
	private String fromVlanMapping;
	private String toVlanMapping;
	private String fromVlanType;
	private String toVlanType;
	private List<ServiceVLANIdRange> fromPortVLANIdRange;
	private List<ServiceVLANIdRange> toPortVLANIdRange;
	private String customerName;
	private String ocn;
	private Integer commitmentPeriod;
	private String commitmentExpiryDate;
	private String nms;
	public ConnectionRequestService() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFromPortId() {
		return fromPortId;
	}

	public void setFromPortId(String fromPortId) {
		this.fromPortId = fromPortId;
	}

	public String getToPortId() {
		return toPortId;
	}

	public void setToPortId(String toPortId) {
		this.toPortId = toPortId;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getFromUniType() {
		return fromUniType;
	}

	public void setFromUniType(String fromUniType) {
		this.fromUniType = fromUniType;
	}

	public String getToUniType() {
		return toUniType;
	}

	public void setToUniType(String toUniType) {
		this.toUniType = toUniType;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getConnectionType() {
		return connectionType;
	}

	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getFromVlanMapping() {
		return fromVlanMapping;
	}

	public void setFromVlanMapping(String fromVlanMapping) {
		this.fromVlanMapping = fromVlanMapping;
	}

	public String getToVlanMapping() {
		return toVlanMapping;
	}

	public void setToVlanMapping(String toVlanMapping) {
		this.toVlanMapping = toVlanMapping;
	}

	public String getFromVlanType() {
		return fromVlanType;
	}

	public void setFromVlanType(String fromVlanType) {
		this.fromVlanType = fromVlanType;
	}

	public String getToVlanType() {
		return toVlanType;
	}

	public void setToVlanType(String toVlanType) {
		this.toVlanType = toVlanType;
	}

	public List<ServiceVLANIdRange> getFromPortVLANIdRange() {
		return fromPortVLANIdRange;
	}

	public void setFromPortVLANIdRange(List<ServiceVLANIdRange> fromPortVLANIdRange) {
		this.fromPortVLANIdRange = fromPortVLANIdRange;
	}

	public List<ServiceVLANIdRange> getToPortVLANIdRange() {
		return toPortVLANIdRange;
	}

	public void setToPortVLANIdRange(List<ServiceVLANIdRange> toPortVLANIdRange) {
		this.toPortVLANIdRange = toPortVLANIdRange;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}

	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}
	

	public String getNms() {
		return nms;
	}

	public void setNms(String nms) {
		this.nms = nms;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ConnectionRequestService [name=" + name + ", fromPortId="
				+ fromPortId + ", toPortId=" + toPortId + ", resourceId="
				+ resourceId + ", fromUniType=" + fromUniType + ", toUniType="
				+ toUniType + ", bandwidth=" + bandwidth + ", rentalCharge="
				+ rentalCharge + ", rentalUnit=" + rentalUnit
				+ ", rentalCurrency=" + rentalCurrency + ", connectionType="
				+ connectionType + ", serviceId=" + serviceId
				+ ", fromVlanMapping=" + fromVlanMapping + ", toVlanMapping="
				+ toVlanMapping + ", fromVlanType=" + fromVlanType
				+ ", toVlanType=" + toVlanType + ", fromPortVLANIdRange="
				+ fromPortVLANIdRange + ", toPortVLANIdRange="
				+ toPortVLANIdRange + ", customerName=" + customerName
				+ ", ocn=" + ocn + ", commitmentPeriod=" + commitmentPeriod
				+ ", commitmentExpiryDate=" + commitmentExpiryDate + "]";
	}

	

}
