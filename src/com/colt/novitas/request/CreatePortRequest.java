package com.colt.novitas.request;

import java.io.Serializable;
import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class CreatePortRequest implements Serializable {

	private static final long serialVersionUID = -7385233083425327811L;

	private String name;
	private String location;
	private Integer bandwidth;
	private String connector;
	private String technology;
	private String presentationLabel;
	private Float rentalCharge;
	private String rentalUnit;
	private String rentalCurrency;
	private String address;
	private String siteType;
	private Date commitmentExpiry;
	private String serviceId;
	private String resourceId;
	private Float decommissioningCharge;
	private String decommissioningCurrency;
	private String customerName;
	private String ocn;
	private String siteFloor;
	private String siteRoomName;
	private String locationPremisesNumber;
	private String locationBuildingName;
	private String locationStreetName;
	private String locationCity;
	private String locationState;
	private String locationCountry;
	private String postalZipCode;
	private Float latitude;
	private Float longitude;
	private String locationId;
	private String portType;

	private Integer portExpiryPeriod;
	private String portExpiresOn;
	private Boolean isPortInUse;
	private boolean loaAllowed;
	private String localBuildingName;

	private String locationCityCode;
	private String locationCountryCode;
	private String resourcePortName;
	private Boolean isCrossConnectOrderable;
	private String crossConnectRequestId;

	@SerializedName("customer_port_id")
	private String customerPortID;
	private Integer commitmentPeriod;
	private Integer maxAllowedIpaConnBandwidth;

	public CreatePortRequest() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public String getPresentationLabel() {
		return presentationLabel;
	}

	public void setPresentationLabel(String presentationLabel) {
		this.presentationLabel = presentationLabel;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public Date getCommitmentExpiry() {
		return commitmentExpiry;
	}

	public void setCommitmentExpiry(Date commitmentExpiry) {
		this.commitmentExpiry = commitmentExpiry;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	/**
	 * @return the locationBuildingName
	 */
	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	/**
	 * @param locationBuildingName
	 *            the locationBuildingName to set
	 */
	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	/**
	 * @return the locationStreetName
	 */
	public String getLocationStreetName() {
		return locationStreetName;
	}

	/**
	 * @param locationStreetName
	 *            the locationStreetName to set
	 */
	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	/**
	 * @return the locationCity
	 */
	public String getLocationCity() {
		return locationCity;
	}

	/**
	 * @param locationCity
	 *            the locationCity to set
	 */
	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	/**
	 * @return the locationCountry
	 */
	public String getLocationCountry() {
		return locationCountry;
	}

	/**
	 * @param locationCountry
	 *            the locationCountry to set
	 */
	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	/**
	 * @return the latitude
	 */
	public Float getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public Float getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the siteFloor
	 */
	public String getSiteFloor() {
		return siteFloor;
	}

	/**
	 * @param siteFloor
	 *            the siteFloor to set
	 */
	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	/**
	 * @return the siteRoomName
	 */
	public String getSiteRoomName() {
		return siteRoomName;
	}

	/**
	 * @param siteRoomName
	 *            the siteRoomName to set
	 */
	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	/**
	 * @return the locationPremisesNumber
	 */
	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	/**
	 * @param locationPremisesNumber
	 *            the locationPremisesNumber to set
	 */
	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}

	/**
	 * @return the locationState
	 */
	public String getLocationState() {
		return locationState;
	}

	/**
	 * @param locationState
	 *            the locationState to set
	 */
	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	/**
	 * @return the postalZipCode
	 */
	public String getPostalZipCode() {
		return postalZipCode;
	}

	/**
	 * @param postalZipCode
	 *            the postalZipCode to set
	 */
	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	/**
	 * @return the locationId
	 */
	public String getLocationId() {
		return locationId;
	}

	/**
	 * @param locationId
	 *            the locationId to set
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getPortType() {
		return portType;
	}

	public void setPortType(String portType) {
		this.portType = portType;
	}

	/**
	 * @return the portExpiryPeriod
	 */
	public Integer getPortExpiryPeriod() {
		return portExpiryPeriod;
	}

	/**
	 * @param portExpiryPeriod
	 *            the portExpiryPeriod to set
	 */
	public void setPortExpiryPeriod(Integer portExpiryPeriod) {
		this.portExpiryPeriod = portExpiryPeriod;
	}

	/**
	 * @return the portExpiresOn
	 */
	public String getPortExpiresOn() {
		return portExpiresOn;
	}

	/**
	 * @param portExpiresOn
	 *            the portExpiresOn to set
	 */
	public void setPortExpiresOn(String portExpiresOn) {
		this.portExpiresOn = portExpiresOn;
	}

	/**
	 * @return the isPortInUse
	 */
	public Boolean getIsPortInUse() {
		return isPortInUse;
	}

	/**
	 * @param isPortInUse
	 *            the isPortInUse to set
	 */
	public void setIsPortInUse(Boolean isPortInUse) {
		this.isPortInUse = isPortInUse;
	}

	public boolean isLoaAllowed() {
		return loaAllowed;
	}

	public void setLoaAllowed(boolean loaAllowed) {
		this.loaAllowed = loaAllowed;
	}

	public String getLocalBuildingName() {
		return localBuildingName;
	}

	public void setLocalBuildingName(String localBuildingName) {
		this.localBuildingName = localBuildingName;
	}

	public String getLocationCityCode() {
		return locationCityCode;
	}

	public void setLocationCityCode(String locationCityCode) {
		this.locationCityCode = locationCityCode;
	}

	public String getLocationCountryCode() {
		return locationCountryCode;
	}

	public void setLocationCountryCode(String locationCountryCode) {
		this.locationCountryCode = locationCountryCode;
	}

	public String getResourcePortName() {
		return resourcePortName;
	}

	public void setResourcePortName(String resourcePortName) {
		this.resourcePortName = resourcePortName;
	}

	public Boolean getIsCrossConnectOrderable() {
		return isCrossConnectOrderable;
	}

	public void setIsCrossConnectOrderable(Boolean isCrossConnectOrderable) {
		this.isCrossConnectOrderable = isCrossConnectOrderable;
	}

	public String getCrossConnectRequestId() {
		return crossConnectRequestId;
	}

	public void setCrossConnectRequestId(String crossConnectRequestId) {
		this.crossConnectRequestId = crossConnectRequestId;
	}

	public String getCustomerPortID() {
		return customerPortID;
	}

	public void setCustomerPortID(String customerPortID) {
		this.customerPortID = customerPortID;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public Integer getMaxAllowedIpaConnBandwidth() {
		return maxAllowedIpaConnBandwidth;
	}

	public void setMaxAllowedIpaConnBandwidth(Integer maxAllowedIpaConnBandwidth) {
		this.maxAllowedIpaConnBandwidth = maxAllowedIpaConnBandwidth;
	}

	@Override
	public String toString() {
		return "CreatePortRequest [name=" + name + ", location=" + location + ", bandwidth=" + bandwidth
				+ ", connector=" + connector + ", technology=" + technology + ", presentationLabel=" + presentationLabel
				+ ", rentalCharge=" + rentalCharge + ", rentalUnit=" + rentalUnit + ", rentalCurrency=" + rentalCurrency
				+ ", address=" + address + ", siteType=" + siteType + ", commitmentExpiry=" + commitmentExpiry
				+ ", serviceId=" + serviceId + ", resourceId=" + resourceId + ", decommissioningCharge="
				+ decommissioningCharge + ", decommissioningCurrency=" + decommissioningCurrency + ", customerName="
				+ customerName + ", ocn=" + ocn + ", siteFloor=" + siteFloor + ", siteRoomName=" + siteRoomName
				+ ", locationPremisesNumber=" + locationPremisesNumber + ", locationBuildingName="
				+ locationBuildingName + ", locationStreetName=" + locationStreetName + ", locationCity=" + locationCity
				+ ", locationState=" + locationState + ", locationCountry=" + locationCountry + ", postalZipCode="
				+ postalZipCode + ", latitude=" + latitude + ", longitude=" + longitude + ", locationId=" + locationId
				+ ", portType=" + portType + ", portExpiryPeriod=" + portExpiryPeriod + ", portExpiresOn="
				+ portExpiresOn + ", isPortInUse=" + isPortInUse + ", loaAllowed=" + loaAllowed + ", localBuildingName="
				+ localBuildingName + ", locationCityCode=" + locationCityCode + ", locationCountryCode="
				+ locationCountryCode + ", resourcePortName=" + resourcePortName + ", isCrossConnectOrderable="
				+ isCrossConnectOrderable + ", crossConnectRequestId=" + crossConnectRequestId + ", customerPortID="
				+ customerPortID + ", commitmentPeriod=" + commitmentPeriod + ", maxAllowedIpaConnBandwidth="
				+ maxAllowedIpaConnBandwidth + "]";
	}

}