package com.colt.novitas.request;

import java.io.Serializable;
import java.util.List;

import com.colt.novitas.response.ConnectionPortPair;
import com.google.gson.annotations.SerializedName;

public class UpdateCloudConnRequest implements Serializable {

	private static final long serialVersionUID = -5886828712798157081L;

	@SerializedName("status")
	private String status;

	@SerializedName("resource_id_1")
	private String resourceId1;

	@SerializedName("resource_id_2")
	private String resourceId2;

	@SerializedName("bandwidth")
	private Integer bandwidth;

	@SerializedName("rental_charge")
	private Float rentalCharge;

	@SerializedName("rental_unit")
	private String rentalUnit;

	@SerializedName("rental_currency")
	private String rentalCurrency;

	@SerializedName("commitment_period")
	private Integer commitmentPeriod;

	@SerializedName("commitment_expiry_date")
	private String commitmentExpiryDate;

	@SerializedName("component_connections")
	private List<ConnectionPortPair> ports;

	@SerializedName("nc_tech_service_id_1")
	private String ncTechServiceId1;

	@SerializedName("nc_tech_service_id_2")
	private String ncTechServiceId2;

	@SerializedName("base_bandwidth")
	private Integer baseBandwidth;

	@SerializedName("base_rental")
	private Float baseRental;

	public UpdateCloudConnRequest() {
		super();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}

	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}

	/**
	 * @return the resourceId1
	 */
	public String getResourceId1() {
		return resourceId1;
	}

	/**
	 * @param resourceId1
	 *            the resourceId1 to set
	 */
	public void setResourceId1(String resourceId1) {
		this.resourceId1 = resourceId1;
	}

	/**
	 * @return the resourceId2
	 */
	public String getResourceId2() {
		return resourceId2;
	}

	/**
	 * @param resourceId2
	 *            the resourceId2 to set
	 */
	public void setResourceId2(String resourceId2) {
		this.resourceId2 = resourceId2;
	}

	/**
	 * @return the ports
	 */
	public List<ConnectionPortPair> getPorts() {
		return ports;
	}

	/**
	 * @param ports
	 *            the ports to set
	 */
	public void setPorts(List<ConnectionPortPair> ports) {
		this.ports = ports;
	}

	public String getNcTechServiceId1() {
		return ncTechServiceId1;
	}

	public void setNcTechServiceId1(String ncTechServiceId1) {
		this.ncTechServiceId1 = ncTechServiceId1;
	}

	public String getNcTechServiceId2() {
		return ncTechServiceId2;
	}

	public void setNcTechServiceId2(String ncTechServiceId2) {
		this.ncTechServiceId2 = ncTechServiceId2;
	}

	public Integer getBaseBandwidth() {
		return baseBandwidth;
	}

	public void setBaseBandwidth(Integer baseBandwidth) {
		this.baseBandwidth = baseBandwidth;
	}

	public Float getBaseRental() {
		return baseRental;
	}

	public void setBaseRental(Float baseRental) {
		this.baseRental = baseRental;
	}

	@Override
	public String toString() {
		return "UpdateCloudConnRequest{" +
				"status='" + status + '\'' +
				", resourceId1='" + resourceId1 + '\'' +
				", resourceId2='" + resourceId2 + '\'' +
				", bandwidth=" + bandwidth +
				", rentalCharge=" + rentalCharge +
				", rentalUnit='" + rentalUnit + '\'' +
				", rentalCurrency='" + rentalCurrency + '\'' +
				", commitmentPeriod=" + commitmentPeriod +
				", commitmentExpiryDate='" + commitmentExpiryDate + '\'' +
				", ports=" + ports +
				", ncTechServiceId1='" + ncTechServiceId1 + '\'' +
				", ncTechServiceId2='" + ncTechServiceId2 + '\'' +
				", baseBandwidth=" + baseBandwidth +
				", baseRental=" + baseRental +
				'}';
	}
}