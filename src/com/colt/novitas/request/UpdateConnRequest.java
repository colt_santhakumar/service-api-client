package com.colt.novitas.request;

import java.io.Serializable;
import java.util.List;

import com.colt.novitas.response.ServiceVLANIdRange;

public class UpdateConnRequest implements Serializable {

	private static final long serialVersionUID = -5886828712798157081L;

	private String status;
	private String resourceId;
	private Integer bandwidth;
	private Float rentalCharge;
	private String rentalCurrency;
	private String rentalUnit;
	private String fromVlanMapping;
	private String toVlanMapping;
	private String fromVlanType;
	private String toVlanType;
	private List<ServiceVLANIdRange> fromPortVLANIdRange;
	private List<ServiceVLANIdRange> toPortVLANIdRange;
	private Integer commitmentPeriod;
	private String commitmentExpiryDate;

	private String oloServiceId;

	private String ncTechServiceId;

	private Integer baseBandwidth;
	private Float baseRental;

	public UpdateConnRequest() {
		super();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getFromVlanMapping() {
		return fromVlanMapping;
	}

	public void setFromVlanMapping(String fromVlanMapping) {
		this.fromVlanMapping = fromVlanMapping;
	}

	public String getToVlanMapping() {
		return toVlanMapping;
	}

	public void setToVlanMapping(String toVlanMapping) {
		this.toVlanMapping = toVlanMapping;
	}

	public String getFromVlanType() {
		return fromVlanType;
	}

	public void setFromVlanType(String fromVlanType) {
		this.fromVlanType = fromVlanType;
	}

	public String getToVlanType() {
		return toVlanType;
	}

	public void setToVlanType(String toVlanType) {
		this.toVlanType = toVlanType;
	}

	public List<ServiceVLANIdRange> getFromPortVLANIdRange() {
		return fromPortVLANIdRange;
	}

	public void setFromPortVLANIdRange(List<ServiceVLANIdRange> fromPortVLANIdRange) {
		this.fromPortVLANIdRange = fromPortVLANIdRange;
	}

	public List<ServiceVLANIdRange> getToPortVLANIdRange() {
		return toPortVLANIdRange;
	}

	public void setToPortVLANIdRange(List<ServiceVLANIdRange> toPortVLANIdRange) {
		this.toPortVLANIdRange = toPortVLANIdRange;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}

	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}

	public String getOloServiceId() {
		return oloServiceId;
	}

	public void setOloServiceId(String oloServiceId) {
		this.oloServiceId = oloServiceId;
	}

	public String getNcTechServiceId() {
		return ncTechServiceId;
	}

	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}

	public Integer getBaseBandwidth() {
		return this.baseBandwidth;
	}

	public Float getBaseRental() {
		return this.baseRental;
	}

	public void setBaseRental(Float baseRental) {
		this.baseRental = baseRental;
	}

	public void setBaseBandwidth(Integer baseBandwidth) {
		this.baseBandwidth = baseBandwidth;
	}

	@Override
	public String toString() {
		return "UpdateConnRequest [status=" + status + ", resourceId=" + resourceId + ", bandwidth=" + bandwidth
				+ ", rentalCharge=" + rentalCharge + ", rentalCurrency=" + rentalCurrency + ", rentalUnit=" + rentalUnit
				+ ", fromVlanMapping=" + fromVlanMapping + ", toVlanMapping=" + toVlanMapping + ", fromVlanType="
				+ fromVlanType + ", toVlanType=" + toVlanType + ", fromPortVLANIdRange=" + fromPortVLANIdRange
				+ ", toPortVLANIdRange=" + toPortVLANIdRange + ", commitmentPeriod=" + commitmentPeriod
				+ ", commitmentExpiryDate=" + commitmentExpiryDate + ", oloServiceId=" + oloServiceId
				+ ", ncTechServiceId=" + ncTechServiceId + ", baseRental=" + baseRental + ", baseBandwidth=" + baseBandwidth + "]";
	}

}