package com.colt.novitas.request;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class UpdateCloudPortRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1772326569297485367L;

	@SerializedName("location")
	private String location;

	@SerializedName("bandwidth")
	private Integer bandwidth;

	@SerializedName("address")
	private String address;

	@SerializedName("site_type")
	private String siteType;

	@SerializedName("site_floor")
	private String siteFloor;

	@SerializedName("site_room_name")
	private String siteRoomName;

	@SerializedName("location_premises_number")
	private String locationPremisesNumber;

	@SerializedName("location_building_name")
	private String locationBuildingName;

	@SerializedName("location_street_name")
	private String locationStreetName;

	@SerializedName("location_city")
	private String locationCity;

	@SerializedName("location_state")
	private String locationState;

	@SerializedName("location_country")
	private String locationCountry;

	@SerializedName("postal_zip_code")
	private String postalZipCode;

	@SerializedName("latitude")
	private Float latitude;

	@SerializedName("longitude")
	private Float longitude;

	@SerializedName("location_id")
	private String locationId;

	@SerializedName("port_1")
	private String port1;

	@SerializedName("port_2")
	private String port2;

	@SerializedName("vlan")
	private Integer vlan;

	@SerializedName("status")
	private String status;

	@SerializedName("resource_id_1")
	private String resourceId1;

	@SerializedName("resource_id_2")
	private String resourceId2;

	@SerializedName("nms")
	private String nms;

	@SerializedName("local_building_name")
	private String localBuildingName;

	@SerializedName("location_city_code")
	private String locationCityCode;
	@SerializedName("location_country_code")
	private String locationCountryCode;

	@SerializedName("resource_port_name_1")
	private String resourcePortName1;

	@SerializedName("resource_port_name_2")
	private String resourcePortName2;

	@SerializedName("nc_tech_service_id_1")
	private String ncTechServiceId1;

	@SerializedName("nc_tech_service_id_2")
	private String ncTechServiceId2;

	public UpdateCloudPortRequest() {
		super();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the resourceId1
	 */
	public String getResourceId1() {
		return resourceId1;
	}

	/**
	 * @param resourceId1
	 *            the resourceId1 to set
	 */
	public void setResourceId1(String resourceId1) {
		this.resourceId1 = resourceId1;
	}

	/**
	 * @return the resourceId2
	 */
	public String getResourceId2() {
		return resourceId2;
	}

	/**
	 * @param resourceId2
	 *            the resourceId2 to set
	 */
	public void setResourceId2(String resourceId2) {
		this.resourceId2 = resourceId2;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}

	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	public String getLocationStreetName() {
		return locationStreetName;
	}

	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getLocationState() {
		return locationState;
	}

	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	public String getLocationCountry() {
		return locationCountry;
	}

	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getPort1() {
		return port1;
	}

	public void setPort1(String port1) {
		this.port1 = port1;
	}

	public String getPort2() {
		return port2;
	}

	public void setPort2(String port2) {
		this.port2 = port2;
	}

	public Integer getVlan() {
		return vlan;
	}

	public void setVlan(Integer vlan) {
		this.vlan = vlan;
	}

	public String getNms() {
		return nms;
	}

	public void setNms(String nms) {
		this.nms = nms;
	}

	public String getLocalBuildingName() {
		return localBuildingName;
	}

	public void setLocalBuildingName(String localBuildingName) {
		this.localBuildingName = localBuildingName;
	}

	public String getLocationCityCode() {
		return locationCityCode;
	}

	public void setLocationCityCode(String locationCityCode) {
		this.locationCityCode = locationCityCode;
	}

	public String getLocationCountryCode() {
		return locationCountryCode;
	}

	public void setLocationCountryCode(String locationCountryCode) {
		this.locationCountryCode = locationCountryCode;
	}

	public String getResourcePortName1() {
		return resourcePortName1;
	}

	public void setResourcePortName1(String resourcePortName1) {
		this.resourcePortName1 = resourcePortName1;
	}

	public String getResourcePortName2() {
		return resourcePortName2;
	}

	public void setResourcePortName2(String resourcePortName2) {
		this.resourcePortName2 = resourcePortName2;
	}

	public String getNcTechServiceId1() {
		return ncTechServiceId1;
	}

	public void setNcTechServiceId1(String ncTechServiceId1) {
		this.ncTechServiceId1 = ncTechServiceId1;
	}

	public String getNcTechServiceId2() {
		return ncTechServiceId2;
	}

	public void setNcTechServiceId2(String ncTechServiceId2) {
		this.ncTechServiceId2 = ncTechServiceId2;
	}

	@Override
	public String toString() {
		return "UpdateCloudPortRequest [location=" + location + ", bandwidth=" + bandwidth + ", address=" + address
				+ ", siteType=" + siteType + ", siteFloor=" + siteFloor + ", siteRoomName=" + siteRoomName
				+ ", locationPremisesNumber=" + locationPremisesNumber + ", locationBuildingName="
				+ locationBuildingName + ", locationStreetName=" + locationStreetName + ", locationCity=" + locationCity
				+ ", locationState=" + locationState + ", locationCountry=" + locationCountry + ", postalZipCode="
				+ postalZipCode + ", latitude=" + latitude + ", longitude=" + longitude + ", locationId=" + locationId
				+ ", port1=" + port1 + ", port2=" + port2 + ", vlan=" + vlan + ", status=" + status + ", resourceId1="
				+ resourceId1 + ", resourceId2=" + resourceId2 + ", nms=" + nms + ", localBuildingName="
				+ localBuildingName + ", locationCityCode=" + locationCityCode + ", locationCountryCode="
				+ locationCountryCode + ", resourcePortName1=" + resourcePortName1 + ", resourcePortName2="
				+ resourcePortName2 + ", ncTechServiceId1=" + ncTechServiceId1 + ", ncTechServiceId2="
				+ ncTechServiceId2 + "]";
	}

}