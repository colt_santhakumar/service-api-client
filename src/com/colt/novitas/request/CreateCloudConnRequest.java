package com.colt.novitas.request;

import java.io.Serializable;
import java.util.List;

import com.colt.novitas.response.ConnectionPortPair;
import com.google.gson.annotations.SerializedName;

public class CreateCloudConnRequest implements Serializable {

    private static final long serialVersionUID = 2766815226223017067L;
    
    @SerializedName("name")
    private String name;
    
    @SerializedName("resource_id_1")
    private String resourceId1;
    
    @SerializedName("resource_id_2")
    private String resourceId2;
   
    @SerializedName("bandwidth")
    private Integer bandwidth;
    
    @SerializedName("rental_charge")
    private Float rentalCharge;
    
    @SerializedName("rental_unit")
    private String rentalUnit;
    
    @SerializedName("rental_currency")
    private String rentalCurrency;
    
    @SerializedName("connection_type")
    private String connectionType;
    
    @SerializedName("service_id")
    private String serviceId;

    @SerializedName("customer_name")
    private String customerName;
    
    @SerializedName("ocn")
    private String ocn;
    
    @SerializedName("commitment_period")
    private Integer commitmentPeriod;
    
    @SerializedName("commitment_expiry_date")
    private String commitmentExpiryDate;
    
    @SerializedName("nms")
    private String nms;

    @SerializedName("component_connections")
    private List<ConnectionPortPair> ports;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(Integer bandwidth) {
        this.bandwidth = bandwidth;
    }

    public Float getRentalCharge() {
        return rentalCharge;
    }

    public void setRentalCharge(Float rentalCharge) {
        this.rentalCharge = rentalCharge;
    }

    public String getRentalUnit() {
        return rentalUnit;
    }

    public void setRentalUnit(String rentalUnit) {
        this.rentalUnit = rentalUnit;
    }

    public String getRentalCurrency() {
        return rentalCurrency;
    }

    public void setRentalCurrency(String rentalCurrency) {
        this.rentalCurrency = rentalCurrency;
    }

    public String getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOcn() {
        return ocn;
    }

    public void setOcn(String ocn) {
        this.ocn = ocn;
    }

    public Integer getCommitmentPeriod() {
        return commitmentPeriod;
    }

    public void setCommitmentPeriod(Integer commitmentPeriod) {
        this.commitmentPeriod = commitmentPeriod;
    }

    public String getCommitmentExpiryDate() {
        return commitmentExpiryDate;
    }

    public void setCommitmentExpiryDate(String commitmentExpiryDate) {
        this.commitmentExpiryDate = commitmentExpiryDate;
    }

    /**
     * @return the resourceId1
     */
    public String getResourceId1() {
        return resourceId1;
    }

    /**
     * @param resourceId1 the resourceId1 to set
     */
    public void setResourceId1(String resourceId1) {
        this.resourceId1 = resourceId1;
    }

    /**
     * @return the resourceId2
     */
    public String getResourceId2() {
        return resourceId2;
    }

    /**
     * @param resourceId2 the resourceId2 to set
     */
    public void setResourceId2(String resourceId2) {
        this.resourceId2 = resourceId2;
    }

    /**
     * @return the ports
     */
    public List<ConnectionPortPair> getPorts() {
        return ports;
    }

    /**
     * @param ports the ports to set
     */
    public void setPorts(List<ConnectionPortPair> ports) {
        this.ports = ports;
    }

	public String getNms() {
		return nms;
	}

	public void setNms(String nms) {
		this.nms = nms;
	}   
	
    

}
