package com.colt.novitas.request;

import java.io.Serializable;

public class UpdatePortRequest implements Serializable {

	private static final long serialVersionUID = 3137739718361666823L;

	private String status;
	private String resourceId;
	private String presentationLabel;
	private String portExpiresOn;
	private Boolean portInUse;
	private String nms;
	private String resourcePortName;
	private String crossConnectRequestId;
	private String crossConnectId;
	private String ncTechServiceId;
	private String commitmentExpiryDate;
	private Integer commitmentPeriod;
	private Integer maxAllowedIpaConnBandwidth;

	public UpdatePortRequest() {
		super();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getPresentationLabel() {
		return presentationLabel;
	}

	public void setPresentationLabel(String presentationLabel) {
		this.presentationLabel = presentationLabel;
	}

	/**
	 * @return the portExpiresOn
	 */
	public String getPortExpiresOn() {
		return portExpiresOn;
	}

	/**
	 * @param portExpiresOn
	 *            the portExpiresOn to set
	 */
	public void setPortExpiresOn(String portExpiresOn) {
		this.portExpiresOn = portExpiresOn;
	}

	/**
	 * @return the portInUse
	 */
	public Boolean getPortInUse() {
		return portInUse;
	}

	/**
	 * @param portInUse
	 *            the portInUse to set
	 */
	public void setPortInUse(Boolean portInUse) {
		this.portInUse = portInUse;
	}

	public String getNms() {
		return nms;
	}

	public void setNms(String nms) {
		this.nms = nms;
	}

	public String getResourcePortName() {
		return resourcePortName;
	}

	public void setResourcePortName(String resourcePortName) {
		this.resourcePortName = resourcePortName;
	}

	public String getCrossConnectRequestId() {
		return crossConnectRequestId;
	}

	public void setCrossConnectRequestId(String crossConnectRequestId) {
		this.crossConnectRequestId = crossConnectRequestId;
	}

	public String getCrossConnectId() {
		return crossConnectId;
	}

	public void setCrossConnectId(String crossConnectId) {
		this.crossConnectId = crossConnectId;
	}

	public String getNcTechServiceId() {
		return ncTechServiceId;
	}

	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}

	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}

	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public Integer getMaxAllowedIpaConnBandwidth() {
		return maxAllowedIpaConnBandwidth;
	}

	public void setMaxAllowedIpaConnBandwidth(Integer maxAllowedIpaConnBandwidth) {
		this.maxAllowedIpaConnBandwidth = maxAllowedIpaConnBandwidth;
	}

	@Override
	public String toString() {
		return "UpdatePortRequest [status=" + status + ", resourceId=" + resourceId + ", presentationLabel="
				+ presentationLabel + ", portExpiresOn=" + portExpiresOn + ", portInUse=" + portInUse + ", nms=" + nms
				+ ", resourcePortName=" + resourcePortName + ", crossConnectRequestId=" + crossConnectRequestId
				+ ", crossConnectId=" + crossConnectId + ", ncTechServiceId=" + ncTechServiceId
				+ ", commitmentExpiryDate=" + commitmentExpiryDate + ", commitmentPeriod=" + commitmentPeriod
				+ ", maxAllowedIpaConnBandwidth=" + maxAllowedIpaConnBandwidth + "]";
	}

}
