package com.colt.novitas.request;

import java.io.Serializable;
import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class CreateCloudPortRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2104343630728333762L;

	@SerializedName("name")
	private String name;

	@SerializedName("location")
	private String location;

	@SerializedName("bandwidth")
	private Integer bandwidth;

	@SerializedName("rental_charge")
	private Float rentalCharge;

	@SerializedName("rental_unit")
	private String rentalUnit;

	@SerializedName("rental_currency")
	private String rentalCurrency;

	@SerializedName("address")
	private String address;

	@SerializedName("site_type")
	private String siteType;

	@SerializedName("commitment_expiry")
	private Date commitmentExpiry;

	@SerializedName("service_id")
	private String serviceId;

	@SerializedName("decommissioning_charge")
	private Float decommissioningCharge;

	@SerializedName("decommissioning_currency")
	private String decommissioningCurrency;

	@SerializedName("installation_charge")
	protected Float installationCharge;

	@SerializedName("installation_currency")
	protected String installationCurrency;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("ocn")
	private String ocn;

	@SerializedName("site_floor")
	private String siteFloor;

	@SerializedName("site_room_name")
	private String siteRoomName;

	@SerializedName("location_premises_number")
	private String locationPremisesNumber;

	@SerializedName("location_building_name")
	private String locationBuildingName;

	@SerializedName("location_street_name")
	private String locationStreetName;

	@SerializedName("location_city")
	private String locationCity;

	@SerializedName("location_state")
	private String locationState;

	@SerializedName("location_country")
	private String locationCountry;

	@SerializedName("postal_zip_code")
	private String postalZipCode;

	@SerializedName("latitude")
	private Float latitude;

	@SerializedName("longitude")
	private Float longitude;

	@SerializedName("location_id")
	private String locationId;

	@SerializedName("cloud_provider")
	private String cloudProvider;

	@SerializedName("azure_service_key")
	private String serviceKey;

	@SerializedName("port_1")
	private String port1;

	@SerializedName("port_2")
	private String port2;

	@SerializedName("vlan")
	private Integer vlan;

	@SerializedName("resource_id_1")
	private String resourceId1;

	@SerializedName("resource_id_2")
	private String resourceId2;

	/*
	 * @SerializedName("expiration_period") private Integer portExpiryPeriod;
	 * 
	 * @SerializedName("expires_on") private String portExpiresOn;
	 * 
	 * @SerializedName("in_use") private Boolean isPortInUse;
	 */

	@SerializedName("local_building_name")
	private String localBuildingName;

	@SerializedName("location_city_code")
	private String locationCityCode;
	@SerializedName("location_country_code")
	private String locationCountryCode;
	
	@SerializedName("resource_port_name_1")
	private String resourcePortName1;
    
	@SerializedName("resource_port_name_2")
   	private String resourcePortName2;

	public CreateCloudPortRequest() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public Date getCommitmentExpiry() {
		return commitmentExpiry;
	}

	public void setCommitmentExpiry(Date commitmentExpiry) {
		this.commitmentExpiry = commitmentExpiry;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}

	public String getLocationState() {
		return locationState;
	}

	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	public String getLocationStreetName() {
		return locationStreetName;
	}

	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getLocationCountry() {
		return locationCountry;
	}

	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return the cloudProvider
	 */
	public String getCloudProvider() {
		return cloudProvider;
	}

	/**
	 * @param cloudProvider
	 *            the cloudProvider to set
	 */
	public void setCloudProvider(String cloudProvider) {
		this.cloudProvider = cloudProvider;
	}

	/**
	 * @return the serviceKey
	 */
	public String getServiceKey() {
		return serviceKey;
	}

	/**
	 * @param serviceKey
	 *            the serviceKey to set
	 */
	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}

	public String getPort1() {
		return port1;
	}

	public void setPort1(String port1) {
		this.port1 = port1;
	}

	public String getPort2() {
		return port2;
	}

	public void setPort2(String port2) {
		this.port2 = port2;
	}

	/**
	 * @return the vlan
	 */
	public Integer getVlan() {
		return vlan;
	}

	/**
	 * @param vlan
	 *            the Vlan to set
	 */
	public void setVlan(Integer vlan) {
		this.vlan = vlan;
	}

	/**
	 * @return the resourceId1
	 */
	public String getResourceId1() {
		return resourceId1;
	}

	/**
	 * @param resourceId1
	 *            the resourceId1 to set
	 */
	public void setResourceId1(String resourceId1) {
		this.resourceId1 = resourceId1;
	}

	/**
	 * @return the resourceId2
	 */
	public String getResourceId2() {
		return resourceId2;
	}

	/**
	 * @param resourceId2
	 *            the resourceId2 to set
	 */
	public void setResourceId2(String resourceId2) {
		this.resourceId2 = resourceId2;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public String getInstallationCurrency() {
		return installationCurrency;
	}

	public void setInstallationCurrency(String installationCurrency) {
		this.installationCurrency = installationCurrency;
	}

	public String getLocalBuildingName() {
		return localBuildingName;
	}

	public void setLocalBuildingName(String localBuildingName) {
		this.localBuildingName = localBuildingName;
	}

	public String getLocationCityCode() {
		return locationCityCode;
	}

	public void setLocationCityCode(String locationCityCode) {
		this.locationCityCode = locationCityCode;
	}

	public String getLocationCountryCode() {
		return locationCountryCode;
	}

	public void setLocationCountryCode(String locationCountryCode) {
		this.locationCountryCode = locationCountryCode;
	}

	public String getResourcePortName1() {
		return resourcePortName1;
	}

	public void setResourcePortName1(String resourcePortName1) {
		this.resourcePortName1 = resourcePortName1;
	}

	public String getResourcePortName2() {
		return resourcePortName2;
	}

	public void setResourcePortName2(String resourcePortName2) {
		this.resourcePortName2 = resourcePortName2;
	}

	@Override
	public String toString() {
		return "CreateCloudPortRequest [name=" + name + ", location=" + location + ", bandwidth=" + bandwidth
				+ ", rentalCharge=" + rentalCharge + ", rentalUnit=" + rentalUnit + ", rentalCurrency=" + rentalCurrency
				+ ", address=" + address + ", siteType=" + siteType + ", commitmentExpiry=" + commitmentExpiry
				+ ", serviceId=" + serviceId + ", decommissioningCharge=" + decommissioningCharge
				+ ", decommissioningCurrency=" + decommissioningCurrency + ", installationCharge=" + installationCharge
				+ ", installationCurrency=" + installationCurrency + ", customerName=" + customerName + ", ocn=" + ocn
				+ ", siteFloor=" + siteFloor + ", siteRoomName=" + siteRoomName + ", locationPremisesNumber="
				+ locationPremisesNumber + ", locationBuildingName=" + locationBuildingName + ", locationStreetName="
				+ locationStreetName + ", locationCity=" + locationCity + ", locationState=" + locationState
				+ ", locationCountry=" + locationCountry + ", postalZipCode=" + postalZipCode + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", locationId=" + locationId + ", cloudProvider=" + cloudProvider
				+ ", serviceKey=" + serviceKey + ", port1=" + port1 + ", port2=" + port2 + ", vlan=" + vlan
				+ ", resourceId1=" + resourceId1 + ", resourceId2=" + resourceId2 + ", localBuildingName="
				+ localBuildingName + ", locationCityCode=" + locationCityCode + ", locationCountryCode="
				+ locationCountryCode + ", resourcePortName1=" + resourcePortName1 + ", resourcePortName2="
				+ resourcePortName2 + "]";
	}

    

}
