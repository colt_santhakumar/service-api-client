package com.colt.novitas.response;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * {
      "from_port_id": "80000239",
      "to_port_id": "80000299",
      "cloud_provider_from_port": "AWS",
      "cloud_provider_to_port": "AZURE_EXPRESS"
    }
 * @author omerio
 *
 */
public class ConnectionPortPair implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 598578755281264046L;

	// used only for create
    @SerializedName("from_port_id")
    private String fromPortId;
    
    @SerializedName("to_port_id")
    private String toPortId;
    
    // response only (readonly)
    @SerializedName("from_port_name")
    private String fromPortName;
    
    @SerializedName("to_port_name")
    private String toPortName;
    
    @SerializedName("cloud_provider_from_port")
    private String cloudProviderFromPort;
    
    @SerializedName("cloud_provider_to_port")
    private String cloudProviderToPort;
    
    @SerializedName("a_end_vlan_mapping")
    private String aEndVlanMapping;
    
    @SerializedName("b_end_vlan_mapping")
    private String bEndVlanMapping;
    
    @SerializedName("a_end_vlan_type")
    private String aEndVlanType;
    
    @SerializedName("b_end_vlan_type")
    private String bEndVlanType;
    
    @SerializedName("a_end_vlan_ids")
    private List<ServiceVLANIdRange> aEndVlanIds;
    
    @SerializedName("b_end_vlan_ids")
    private List<ServiceVLANIdRange> bEndVlanIds;
    
    
    
    public ConnectionPortPair() {
        super();
    }
   
    public String getFromPortId() {
        return fromPortId;
    }

    public void setFromPortId(String fromPortId) {
        this.fromPortId = fromPortId;
    }

    public String getToPortId() {
        return toPortId;
    }

    public void setToPortId(String toPortId) {
        this.toPortId = toPortId;
    }
    
    public String getaEndVlanMapping() {
        return aEndVlanMapping;
    }

    public void setaEndVlanMapping(String aEndVlanMapping) {
        this.aEndVlanMapping = aEndVlanMapping;
    }

    public String getbEndVlanMapping() {
        return bEndVlanMapping;
    }

    public void setbEndVlanMapping(String bEndVlanMapping) {
        this.bEndVlanMapping = bEndVlanMapping;
    }

    public String getaEndVlanType() {
        return aEndVlanType;
    }

    public void setaEndVlanType(String aEndVlanType) {
        this.aEndVlanType = aEndVlanType;
    }

    public String getbEndVlanType() {
        return bEndVlanType;
    }

    public void setbEndVlanType(String bEndVlanType) {
        this.bEndVlanType = bEndVlanType;
    }

    public List<ServiceVLANIdRange> getaEndVlanIds() {
        return aEndVlanIds;
    }

    public void setaEndVlanIds(List<ServiceVLANIdRange> aEndVlanIds) {
        this.aEndVlanIds = aEndVlanIds;
    }

    public List<ServiceVLANIdRange> getbEndVlanIds() {
        return bEndVlanIds;
    }

    public void setbEndVlanIds(List<ServiceVLANIdRange> bEndVlanIds) {
        this.bEndVlanIds = bEndVlanIds;
    }

    public String getCloudProviderFromPort() {
        return cloudProviderFromPort;
    }

    public void setCloudProviderFromPort(String cloudProviderFromPort) {
        this.cloudProviderFromPort = cloudProviderFromPort;
    }

    public String getCloudProviderToPort() {
        return cloudProviderToPort;
    }

    public void setCloudProviderToPort(String cloudProviderToPort) {
        this.cloudProviderToPort = cloudProviderToPort;
    }
    
    public String getFromPortName() {
        return fromPortName;
    }

    public void setFromPortName(String fromPortName) {
        this.fromPortName = fromPortName;
    }


    public String getToPortName() {
        return toPortName;
    }

    public void setToPortName(String toPortName) {
        this.toPortName = toPortName;
    }

	@Override
	public String toString() {
		return "ConnectionPortPair [fromPortId=" + fromPortId + ", toPortId="
				+ toPortId + ", fromPortName=" + fromPortName + ", toPortName="
				+ toPortName + ", cloudProviderFromPort="
				+ cloudProviderFromPort + ", cloudProviderToPort="
				+ cloudProviderToPort + ", aEndVlanMapping=" + aEndVlanMapping
				+ ", bEndVlanMapping=" + bEndVlanMapping + ", aEndVlanType="
				+ aEndVlanType + ", bEndVlanType=" + bEndVlanType
				+ ", aEndVlanIds=" + aEndVlanIds + ", bEndVlanIds="
				+ bEndVlanIds + "]";
	}
    
    

}
