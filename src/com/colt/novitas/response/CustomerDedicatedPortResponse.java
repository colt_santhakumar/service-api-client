package com.colt.novitas.response;

import java.io.Serializable;

import com.colt.novitas.constants.NetworkPlatform;
import com.colt.novitas.constants.PortStatus;
import com.colt.novitas.constants.RentalUnit;
import com.google.gson.annotations.SerializedName;

public class CustomerDedicatedPortResponse implements Serializable {

	private static final long serialVersionUID = 5583765506647230473L;

	@SerializedName("id")
	private String id;
	@SerializedName("name")
	private String name;
	@SerializedName("status")
	private PortStatus status;
	@SerializedName("location")
	private String location;
	@SerializedName("used_bandwidth")
	private Integer usedBandwidth;
	@SerializedName("available_bandwidth")
	private Integer availableBandwidth;
	@SerializedName("bandwidth")
	private Integer bandwidth;
	@SerializedName("no_of_connections")
	private Integer noOfConnections;
	@SerializedName("connector")
	private String connector;
	@SerializedName("technology")
	private String technology;
	@SerializedName("presentation_label")
	private String presentationLabel;
	@SerializedName("rental_charge")
	private Float rentalCharge;
	@SerializedName("rental_unit")
	private RentalUnit rentalUnit;
	@SerializedName("rental_currency")
	private String rentalCurrency;
	@SerializedName("address")
	private String address;
	@SerializedName("site_type")
	private String siteType;
	@SerializedName("commitment_expiry")
	private String commitmentExpiry;
	@SerializedName("penalty_charge")
	private Float penaltyCharge;
	@SerializedName("service_id")
	private String serviceId;
	@SerializedName("resource_id")
	private String resourceId;
	@SerializedName("customer_name")
	private String customerName;
	@SerializedName("ocn")
	private String ocn;
	@SerializedName("site_floor")
	private String siteFloor;
	@SerializedName("site_room_name")
	private String siteRoomName;
	@SerializedName("location_premises_number")
	private String locationPremisesNumber;
	@SerializedName("location_building_name")
	private String locationBuildingName;
	@SerializedName("location_street_name")
	private String locationStreetName;
	@SerializedName("location_city")
	private String locationCity;
	@SerializedName("location_state")
	private String locationState;
	@SerializedName("location_country")
	private String locationCountry;
	@SerializedName("postal_zip_code")
	private String postalZipCode;
	@SerializedName("latitude")
	private Float latitude;
	@SerializedName("longitude")
	private Float longitude;
	@SerializedName("port_type")
	private String portType;
	@SerializedName(value = "created_date")
	private String createdDate;

	@SerializedName(value = "expiration_period")
	private Integer expirationPeriod;

	@SerializedName(value = "expires_on")
	private String expiresOn;

	@SerializedName(value = "in_use")
	private Boolean inUse;

	@SerializedName(value = "is_hybrid")
	private Boolean isHybrid;

	@SerializedName("decommissioning_charge")
	private Float decommissioningCharge;

	@SerializedName("decommissioning_currency")
	private String decommissioningCurrency;

	@SerializedName("nms")
	private String nms;

	@SerializedName("local_building_name")
	private String localBuildingName;

	@SerializedName("location_city_code")
	private String locationCityCode;
	@SerializedName("location_country_code")
	private String locationCountryCode;
	@SerializedName("resource_port_name")
	private String resourcePortName;
	
	@SerializedName("olo_port_id")
	private String oloPortId;
	
	@SerializedName("cross_connect_id")
	private String crossConnectId;

	@SerializedName("cross_connect_request_id")
	private String crossConnectRequestId;
    
	@SerializedName("nc_tech_service_id")
    private String ncTechServiceId;
	
	@SerializedName("network_platform")
	private NetworkPlatform networkPlatform;

	@SerializedName("max_allowed_ipa_conn_bandwidth")
	private Integer maxAllowedIpaConnBandwidth;

	public CustomerDedicatedPortResponse() {
		super();
	}

	public String getNms() {
		return nms;
	}

	public void setNms(String nms) {
		this.nms = nms;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PortStatus getStatus() {
		return status;
	}

	public void setStatus(PortStatus status) {
		this.status = status;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getUsedBandwidth() {
		return usedBandwidth;
	}

	public void setUsedBandwidth(Integer usedBandwidth) {
		this.usedBandwidth = usedBandwidth;
	}

	public Integer getAvailableBandwidth() {
		return availableBandwidth;
	}

	public void setAvailableBandwidth(Integer availableBandwidth) {
		this.availableBandwidth = availableBandwidth;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public Integer getNoOfConnections() {
		return noOfConnections;
	}

	public void setNoOfConnections(Integer noOfConnections) {
		this.noOfConnections = noOfConnections;
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public String getPresentationLabel() {
		return presentationLabel;
	}

	public void setPresentationLabel(String presentationLabel) {
		this.presentationLabel = presentationLabel;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public RentalUnit getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(RentalUnit rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public String getCommitmentExpiry() {
		return commitmentExpiry;
	}

	public void setCommitmentExpiry(String commitmentExpiry) {
		this.commitmentExpiry = commitmentExpiry;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	/**
	 * @return the locationBuildingName
	 */
	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	/**
	 * @param locationBuildingName
	 *            the locationBuildingName to set
	 */
	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	/**
	 * @return the locationStreetName
	 */
	public String getLocationStreetName() {
		return locationStreetName;
	}

	/**
	 * @param locationStreetName
	 *            the locationStreetName to set
	 */
	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	/**
	 * @return the locationCity
	 */
	public String getLocationCity() {
		return locationCity;
	}

	/**
	 * @param locationCity
	 *            the locationCity to set
	 */
	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	/**
	 * @return the locationCountry
	 */
	public String getLocationCountry() {
		return locationCountry;
	}

	/**
	 * @param locationCountry
	 *            the locationCountry to set
	 */
	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	/**
	 * @return the latitude
	 */
	public Float getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public Float getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the siteFloor
	 */
	public String getSiteFloor() {
		return siteFloor;
	}

	/**
	 * @param siteFloor
	 *            the siteFloor to set
	 */
	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	/**
	 * @return the siteRoomName
	 */
	public String getSiteRoomName() {
		return siteRoomName;
	}

	/**
	 * @param siteRoomName
	 *            the siteRoomName to set
	 */
	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	/**
	 * @return the locationPremisesNumber
	 */
	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	/**
	 * @param locationPremisesNumber
	 *            the locationPremisesNumber to set
	 */
	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}

	/**
	 * @return the locationState
	 */
	public String getLocationState() {
		return locationState;
	}

	/**
	 * @param locationState
	 *            the locationState to set
	 */
	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	/**
	 * @return the postalZipCode
	 */
	public String getPostalZipCode() {
		return postalZipCode;
	}

	/**
	 * @param postalZipCode
	 *            the postalZipCode to set
	 */
	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public String getPortType() {
		return portType;
	}

	public void setPortType(String portType) {
		this.portType = portType;
	}

	public Integer getExpirationPeriod() {
		return expirationPeriod;
	}

	public void setExpirationPeriod(Integer expirationPeriod) {
		this.expirationPeriod = expirationPeriod;
	}

	public String getExpiresOn() {
		return expiresOn;
	}

	public void setExpiresOn(String expiresOn) {
		this.expiresOn = expiresOn;
	}

	public Boolean getInUse() {
		return inUse;
	}

	public void setInUse(Boolean inUse) {
		this.inUse = inUse;
	}

	public Boolean getIsHybrid() {
		return this.isHybrid;
	}

	public void setIsHybrid(Boolean isHybrid) {
		this.isHybrid = isHybrid;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public String getLocalBuildingName() {
		return localBuildingName;
	}

	public void setLocalBuildingName(String localBuildingName) {
		this.localBuildingName = localBuildingName;
	}

	public String getLocationCityCode() {
		return locationCityCode;
	}

	public void setLocationCityCode(String locationCityCode) {
		this.locationCityCode = locationCityCode;
	}

	public String getLocationCountryCode() {
		return locationCountryCode;
	}

	public void setLocationCountryCode(String locationCountryCode) {
		this.locationCountryCode = locationCountryCode;
	}

	public String getResourcePortName() {
		return resourcePortName;
	}

	public void setResourcePortName(String resourcePortName) {
		this.resourcePortName = resourcePortName;
	}

	public String getOloPortId() {
		return oloPortId;
	}

	public void setOloPortId(String oloPortId) {
		this.oloPortId = oloPortId;
	}

	public String getCrossConnectId() {
		return crossConnectId;
	}

	public void setCrossConnectId(String crossConnectId) {
		this.crossConnectId = crossConnectId;
	}

	public String getCrossConnectRequestId() {
		return crossConnectRequestId;
	}

	public void setCrossConnectRequestId(String crossConnectRequestId) {
		this.crossConnectRequestId = crossConnectRequestId;
	}

	public String getNcTechServiceId() {
		return ncTechServiceId;
	}

	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}

	public NetworkPlatform getNetworkPlatform() {
		return networkPlatform;
	}

	public void setNetworkPlatform(NetworkPlatform networkPlatform) {
		this.networkPlatform = networkPlatform;
	}

	public Integer getMaxAllowedIpaConnBandwidth() {
		return maxAllowedIpaConnBandwidth;
	}

	public void setMaxAllowedIpaConnBandwidth(Integer maxAllowedIpaConnBandwidth) {
		this.maxAllowedIpaConnBandwidth = maxAllowedIpaConnBandwidth;
	}

	@Override
	public String toString() {
		return "CustomerDedicatedPortResponse [id=" + id + ", name=" + name + ", status=" + status + ", location="
				+ location + ", usedBandwidth=" + usedBandwidth + ", availableBandwidth=" + availableBandwidth
				+ ", bandwidth=" + bandwidth + ", noOfConnections=" + noOfConnections + ", connector=" + connector
				+ ", technology=" + technology + ", presentationLabel=" + presentationLabel + ", rentalCharge="
				+ rentalCharge + ", rentalUnit=" + rentalUnit + ", rentalCurrency=" + rentalCurrency + ", address="
				+ address + ", siteType=" + siteType + ", commitmentExpiry=" + commitmentExpiry + ", penaltyCharge="
				+ penaltyCharge + ", serviceId=" + serviceId + ", resourceId=" + resourceId + ", customerName="
				+ customerName + ", ocn=" + ocn + ", siteFloor=" + siteFloor + ", siteRoomName=" + siteRoomName
				+ ", locationPremisesNumber=" + locationPremisesNumber + ", locationBuildingName="
				+ locationBuildingName + ", locationStreetName=" + locationStreetName + ", locationCity=" + locationCity
				+ ", locationState=" + locationState + ", locationCountry=" + locationCountry + ", postalZipCode="
				+ postalZipCode + ", latitude=" + latitude + ", longitude=" + longitude + ", portType=" + portType
				+ ", createdDate=" + createdDate + ", expirationPeriod=" + expirationPeriod + ", expiresOn=" + expiresOn
				+ ", inUse=" + inUse + ", isHybrid=" + isHybrid + ", decommissioningCharge=" + decommissioningCharge
				+ ", decommissioningCurrency=" + decommissioningCurrency + ", nms=" + nms + ", localBuildingName="
				+ localBuildingName + ", locationCityCode=" + locationCityCode + ", locationCountryCode="
				+ locationCountryCode + ", resourcePortName=" + resourcePortName + ", oloPortId=" + oloPortId
				+ ", crossConnectId=" + crossConnectId + ", crossConnectRequestId=" + crossConnectRequestId
				+ ", ncTechServiceId=" + ncTechServiceId + ", networkPlatform=" + networkPlatform
				+ ", maxAllowedIpaConnBandwidth=" + maxAllowedIpaConnBandwidth + "]";
	}

}