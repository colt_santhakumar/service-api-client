package com.colt.novitas.response;

import java.io.Serializable;

import com.colt.novitas.constants.ConnectionStatus;
import com.colt.novitas.constants.RentalUnit;
import com.google.gson.annotations.SerializedName;

public class ConnectionBillResponse implements Serializable {

	private static final long serialVersionUID = -6608730009739561046L;

	@SerializedName("connection_id")
	private String connectionId;
	@SerializedName("name")
	private String name;
	@SerializedName("from_port_name")
	private String fromPortName;
	@SerializedName("to_port_name")
	private String toPortName;
	@SerializedName("a_end_address")
	private String aEndAddress;
	@SerializedName("b_end_address")
	private String bEndAddress;
	@SerializedName("bandwidth")
	private Integer bandwidth;
	@SerializedName("created_date")
	private String createdDate;
	@SerializedName("last_updated")
	private String lastUpdated;
	@SerializedName("decommissioned_on")
	private String decommissionedOn;
	@SerializedName("status")
	private ConnectionStatus status;
	@SerializedName("rental_charge")
	private Float rentalCharge;
	@SerializedName("rental_unit")
	private RentalUnit rentalUnit;
	@SerializedName("rental_currency")
	private String rentalCurrency;
	@SerializedName("from_port_id")
	private String fromPortId;
	@SerializedName("to_port_id")
	private String toPortId;
	@SerializedName("service_id")
	private String serviceId;
	@SerializedName("resource_id")
	private String resourceId;
	@SerializedName("from_port_site_type")
	private String fromPortSiteType;
	@SerializedName("to_port_site_type")
	private String toPortSiteType;
	@SerializedName("connection_type")
	private String connectionType;

	public ConnectionBillResponse() {
		super();
	}

	public ConnectionBillResponse(String connectionId, String name, String fromPortName, String toPortName,
			String aEndAddress, String bEndAddress, Integer bandwidth, String createdDate, String lastUpdated,
			String decommissionedOn, ConnectionStatus status, Float rentalCharge, RentalUnit rentalUnit,
			String rentalCurrency, String fromPortId, String toPortId, String serviceId, String resourceId,
			String fromPortSiteType, String toPortSiteType,String connectionType) {
		super();
		this.connectionId = connectionId;
		this.name = name;
		this.fromPortName = fromPortName;
		this.toPortName = toPortName;
		this.aEndAddress = aEndAddress;
		this.bEndAddress = bEndAddress;
		this.bandwidth = bandwidth;
		this.createdDate = createdDate;
		this.lastUpdated = lastUpdated;
		this.decommissionedOn = decommissionedOn;
		this.status = status;
		this.rentalCharge = rentalCharge;
		this.rentalUnit = rentalUnit;
		this.rentalCurrency = rentalCurrency;
		this.fromPortId = fromPortId;
		this.toPortId = toPortId;
		this.serviceId = serviceId;
		this.resourceId = resourceId;
		this.fromPortSiteType = fromPortSiteType;
		this.toPortSiteType = toPortSiteType;
		this.connectionType = connectionType;
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFromPortName() {
		return fromPortName;
	}

	public void setFromPortName(String fromPortName) {
		this.fromPortName = fromPortName;
	}

	public String getToPortName() {
		return toPortName;
	}

	public void setToPortName(String toPortName) {
		this.toPortName = toPortName;
	}

	public String getaEndAddress() {
		return aEndAddress;
	}

	public void setaEndAddress(String aEndAddress) {
		this.aEndAddress = aEndAddress;
	}

	public String getbEndAddress() {
		return bEndAddress;
	}

	public void setbEndAddress(String bEndAddress) {
		this.bEndAddress = bEndAddress;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getDecommissionedOn() {
		return decommissionedOn;
	}

	public void setDecommissionedOn(String decommissionedOn) {
		this.decommissionedOn = decommissionedOn;
	}

	public ConnectionStatus getStatus() {
		return status;
	}

	public void setStatus(ConnectionStatus status) {
		this.status = status;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public RentalUnit getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(RentalUnit rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getFromPortId() {
		return fromPortId;
	}

	public void setFromPortId(String fromPortId) {
		this.fromPortId = fromPortId;
	}

	public String getToPortId() {
		return toPortId;
	}

	public void setToPortId(String toPortId) {
		this.toPortId = toPortId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getFromPortSiteType() {
		return fromPortSiteType;
	}

	public void setFromPortSiteType(String fromPortSiteType) {
		this.fromPortSiteType = fromPortSiteType;
	}

	public String getToPortSiteType() {
		return toPortSiteType;
	}

	public void setToPortSiteType(String toPortSiteType) {
		this.toPortSiteType = toPortSiteType;
	}

	/**
	 * @return the connectionType
	 */
	public String getConnectionType() {
		return connectionType;
	}

	/**
	 * @param connectionType the connectionType to set
	 */
	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ConnectionBillResponse [connectionId=" + connectionId
				+ ", name=" + name + ", fromPortName=" + fromPortName
				+ ", toPortName=" + toPortName + ", aEndAddress=" + aEndAddress
				+ ", bEndAddress=" + bEndAddress + ", bandwidth=" + bandwidth
				+ ", createdDate=" + createdDate + ", lastUpdated="
				+ lastUpdated + ", decommissionedOn=" + decommissionedOn
				+ ", status=" + status + ", rentalCharge=" + rentalCharge
				+ ", rentalUnit=" + rentalUnit + ", rentalCurrency="
				+ rentalCurrency + ", fromPortId=" + fromPortId + ", toPortId="
				+ toPortId + ", serviceId=" + serviceId + ", resourceId="
				+ resourceId + ", fromPortSiteType=" + fromPortSiteType
				+ ", toPortSiteType=" + toPortSiteType + ", connectionType="
				+ connectionType + "]";
	}

	

}