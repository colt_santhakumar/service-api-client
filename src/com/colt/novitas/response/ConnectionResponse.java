package com.colt.novitas.response;

import java.io.Serializable;
import java.util.List;

import com.colt.novitas.constants.ConnectionStatus;
import com.colt.novitas.constants.RentalUnit;
import com.colt.novitas.constants.VlanMapping;
import com.colt.novitas.constants.VlanType;
import com.google.gson.annotations.SerializedName;

public class ConnectionResponse implements Serializable {

	private static final long serialVersionUID = -1705595914057459510L;

	@SerializedName("connection_id")
	private String connectionId;
	@SerializedName("name")
	private String name;
	@SerializedName("from_port_name")
	private String fromPortName;
	@SerializedName("to_port_name")
	private String toPortName;
	@SerializedName("bandwidth")
	private Integer bandwidth;
	@SerializedName("created_date")
	private String createdDate;
	@SerializedName("last_update")
	private String lastUpdated;
	@SerializedName("decommissioned_on")
	private String decommissionedOn;
	@SerializedName("status")
	private ConnectionStatus status;
	@SerializedName("rental_charge")
	private Float rentalCharge;
	@SerializedName("rental_unit")
	private RentalUnit rentalUnit;
	@SerializedName("rental_currency")
	private String rentalCurrency;
	@SerializedName("from_port_id")
	private String fromPortId;
	@SerializedName("to_port_id")
	private String toPortId;
	@SerializedName("service_id")
	private String serviceId;
	@SerializedName("resource_id")
	private String resourceId;
	@SerializedName("a_end_vlan_mapping")
	private VlanMapping fromVlanMapping;
	@SerializedName("b_end_vlan_mapping")
	private VlanMapping toVlanMapping;
	@SerializedName("a_end_vlan_tyep")
	private VlanType fromVlanType;
	@SerializedName("b_end_vlan_type")
	private VlanType toVlanType;
	@SerializedName("a_end_vlan_ids")
	private List<ServiceVLANIdRange> fromPortVLANIdRange;
	@SerializedName("b_end_vlan_ids")
	private List<ServiceVLANIdRange> toPortVLANIdRange;
	@SerializedName("customer_name")
	private String customerName;
	@SerializedName("ocn")
	private String ocn;
	@SerializedName("sla_id")
	private String slaId;
	@SerializedName("penalty_charge")
	private Float penaltyCharge;
	@SerializedName("commitment_period")
	private Integer commitmentPeriod;
	@SerializedName("commitment_expiry_date")
	private String commitmentExpiryDate;
	@SerializedName("connection_type")
	private String connectionType;
	@SerializedName("nms")
	private String nms;
	@SerializedName("nc_tech_service_id")
	private String ncTechServiceId;

	public ConnectionResponse() {
		super();
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFromPortName() {
		return fromPortName;
	}

	public void setFromPortName(String fromPortName) {
		this.fromPortName = fromPortName;
	}

	public String getToPortName() {
		return toPortName;
	}

	public void setToPortName(String toPortName) {
		this.toPortName = toPortName;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getDecommissionedOn() {
		return decommissionedOn;
	}

	public void setDecommissionedOn(String decommissionedOn) {
		this.decommissionedOn = decommissionedOn;
	}

	public ConnectionStatus getStatus() {
		return status;
	}

	public void setStatus(ConnectionStatus status) {
		this.status = status;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public RentalUnit getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(RentalUnit rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getFromPortId() {
		return fromPortId;
	}

	public void setFromPortId(String fromPortId) {
		this.fromPortId = fromPortId;
	}

	public String getToPortId() {
		return toPortId;
	}

	public void setToPortId(String toPortId) {
		this.toPortId = toPortId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public VlanMapping getFromVlanMapping() {
		return fromVlanMapping;
	}

	public void setFromVlanMapping(VlanMapping fromVlanMapping) {
		this.fromVlanMapping = fromVlanMapping;
	}

	public VlanMapping getToVlanMapping() {
		return toVlanMapping;
	}

	public void setToVlanMapping(VlanMapping toVlanMapping) {
		this.toVlanMapping = toVlanMapping;
	}

	public VlanType getFromVlanType() {
		return fromVlanType;
	}

	public void setFromVlanType(VlanType fromVlanType) {
		this.fromVlanType = fromVlanType;
	}

	public VlanType getToVlanType() {
		return toVlanType;
	}

	public void setToVlanType(VlanType toVlanType) {
		this.toVlanType = toVlanType;
	}

	public List<ServiceVLANIdRange> getFromPortVLANIdRange() {
		return fromPortVLANIdRange;
	}

	public void setFromPortVLANIdRange(List<ServiceVLANIdRange> fromPortVLANIdRange) {
		this.fromPortVLANIdRange = fromPortVLANIdRange;
	}

	public List<ServiceVLANIdRange> getToPortVLANIdRange() {
		return toPortVLANIdRange;
	}

	public void setToPortVLANIdRange(List<ServiceVLANIdRange> toPortVLANIdRange) {
		this.toPortVLANIdRange = toPortVLANIdRange;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getSlaId() {
		return slaId;
	}

	public void setSlaId(String slaId) {
		this.slaId = slaId;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}

	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}


	/**
	 * @return the connectionType
	 */
	public String getConnectionType() {
		return connectionType;
	}


	/**
	 * @param connectionType the connectionType to set
	 */
	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}


	public String getNms() {
		return nms;
	}


	public void setNms(String nms) {
		this.nms = nms;
	}


	public String getNcTechServiceId() {
		return ncTechServiceId;
	}


	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}


	@Override
	public String toString() {
		return "ConnectionResponse [connectionId=" + connectionId + ", name=" + name + ", fromPortName=" + fromPortName
				+ ", toPortName=" + toPortName + ", bandwidth=" + bandwidth + ", createdDate=" + createdDate
				+ ", lastUpdated=" + lastUpdated + ", decommissionedOn=" + decommissionedOn + ", status=" + status
				+ ", rentalCharge=" + rentalCharge + ", rentalUnit=" + rentalUnit + ", rentalCurrency=" + rentalCurrency
				+ ", fromPortId=" + fromPortId + ", toPortId=" + toPortId + ", serviceId=" + serviceId + ", resourceId="
				+ resourceId + ", fromVlanMapping=" + fromVlanMapping + ", toVlanMapping=" + toVlanMapping
				+ ", fromVlanType=" + fromVlanType + ", toVlanType=" + toVlanType + ", fromPortVLANIdRange="
				+ fromPortVLANIdRange + ", toPortVLANIdRange=" + toPortVLANIdRange + ", customerName=" + customerName
				+ ", ocn=" + ocn + ", slaId=" + slaId + ", penaltyCharge=" + penaltyCharge + ", commitmentPeriod="
				+ commitmentPeriod + ", commitmentExpiryDate=" + commitmentExpiryDate + ", connectionType="
				+ connectionType + ", nms=" + nms + ", ncTechServiceId=" + ncTechServiceId + "]";
	}


	
    
	
    
	

}
