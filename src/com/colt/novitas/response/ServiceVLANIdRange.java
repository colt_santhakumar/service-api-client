package com.colt.novitas.response;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class ServiceVLANIdRange implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5943217351124354129L;
	
	@SerializedName("from_id_range")
	private Integer fromIdRange;
	
	@SerializedName("to_id_range")
	private Integer toIdRange;

	public ServiceVLANIdRange() {
		super();
	}

	public Integer getFromIdRange() {
		return fromIdRange;
	}

	public void setFromIdRange(Integer fromIdRange) {
		this.fromIdRange = fromIdRange;
	}

	public Integer getToIdRange() {
		return toIdRange;
	}

	public void setToIdRange(Integer toIdRange) {
		this.toIdRange = toIdRange;
	}

	public ServiceVLANIdRange(Integer fromIdRange, Integer toIdRange) {
		super();
		this.fromIdRange = fromIdRange;
		this.toIdRange = toIdRange;
	}

	@Override
	public String toString() {
		return "VLANIdRange [fromIdRange=" + fromIdRange + ", toIdRange=" + toIdRange + "]";
	}

}
