package com.colt.novitas.response;

import java.io.Serializable;

import com.colt.novitas.constants.ConnectionStatus;
import com.colt.novitas.constants.RentalUnit;
import com.google.gson.annotations.SerializedName;

public class CloudConnectionBillResponse implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5103576934043095958L;

	@SerializedName("connection_id")
    private String connectionId;

    @SerializedName("name")
    private String name;

    @SerializedName("cloud_provider_from_port")
    private String cloudProviderFromPort;
    
    @SerializedName("from_port_name_1")
    private String fromPortName1;

    @SerializedName("to_port_name_1")
    private String toPortName1;

    @SerializedName("from_port_name_2")
    private String fromPortName2;

    @SerializedName("to_port_name_2")
    private String toPortName2;

    @SerializedName("a_end_address_1")
    private String aEndAddress1;

    @SerializedName("b_end_address_1")
    private String bEndAddress1;

    @SerializedName("a_end_address_2")
    private String aEndAddress2;

    @SerializedName("b_end_address_2")
    private String bEndAddress2;

    @SerializedName("bandwidth")
    private Integer bandwidth;

    @SerializedName("created_date")
    private String createdDate;

    @SerializedName("last_updated")
    private String lastUpdated;

    @SerializedName("decommissioned_on")
    private String decommissionedOn;

    @SerializedName("status")
    private ConnectionStatus status;

    @SerializedName("rental_charge")
    private Float rentalCharge;

    @SerializedName("rental_unit")
    private RentalUnit rentalUnit;

    @SerializedName("rental_currency")
    private String rentalCurrency;

    @SerializedName("from_port_id_1")
    private String fromPortId1;

    @SerializedName("to_port_id_1")
    private String toPortId1;

    @SerializedName("from_port_id_2")
    private String fromPortId2;

    @SerializedName("to_port_id_2")
    private String toPortId2;

    @SerializedName("service_id")
    private String serviceId;

    @SerializedName("resource_id_1")
    private String resourceId1;

    @SerializedName("resource_id_2")
    private String resourceId2;

    @SerializedName("from_port_site_type_1")
    private String fromPortSiteType1;

    @SerializedName("to_port_site_type_1")
    private String toPortSiteType1;

    @SerializedName("from_port_site_type_2")
    private String fromPortSiteType2;

    @SerializedName("to_port_site_type_2")
    private String toPortSiteType2;

    public CloudConnectionBillResponse() {
        super();
    }



    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getFromPortName1() {
        return fromPortName1;
    }

    public void setFromPortName1(String fromPortName) {
        this.fromPortName1 = fromPortName;
    }


    public String getToPortName1() {
        return toPortName1;
    }

    public void setToPortName1(String toPortName) {
        this.toPortName1 = toPortName;
    }


    public String getaEndAddress1() {
        return aEndAddress1;
    }

    public void setaEndAddress1(String aEndAddress) {
        this.aEndAddress1 = aEndAddress;
    }


    public String getbEndAddress1() {
        return bEndAddress1;
    }

    public void setbEndAddress1(String bEndAddress) {
        this.bEndAddress1 = bEndAddress;
    }

    //-----

    public String getFromPortName2() {
        return fromPortName2;
    }

    public void setFromPortName2(String fromPortName) {
        this.fromPortName2 = fromPortName;
    }


    public String getToPortName2() {
        return toPortName2;
    }

    public void setToPortName2(String toPortName) {
        this.toPortName2 = toPortName;
    }


    public String getaEndAddress2() {
        return aEndAddress2;
    }

    public void setaEndAddress2(String aEndAddress) {
        this.aEndAddress2 = aEndAddress;
    }


    public String getbEndAddress2() {
        return bEndAddress2;
    }

    public void setbEndAddress2(String bEndAddress) {
        this.bEndAddress2 = bEndAddress;
    }


    //-----


    public Integer getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(Integer bandwidth) {
        this.bandwidth = bandwidth;
    }


    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }


    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }


    public String getDecommissionedOn() {
        return decommissionedOn;
    }

    public void setDecommissionedOn(String decommissionedOn) {
        this.decommissionedOn = decommissionedOn;
    }


    public ConnectionStatus getStatus() {
        return status;
    }

    public void setStatus(ConnectionStatus status) {
        this.status = status;
    }


    public Float getRentalCharge() {
        return rentalCharge;
    }

    public void setRentalCharge(Float rentalCharge) {
        this.rentalCharge = rentalCharge;
    }


    public RentalUnit getRentalUnit() {
        return rentalUnit;
    }

    public void setRentalUnit(RentalUnit rentalUnit) {
        this.rentalUnit = rentalUnit;
    }


    public String getRentalCurrency() {
        return rentalCurrency;
    }

    public void setRentalCurrency(String rentalCurrency) {
        this.rentalCurrency = rentalCurrency;
    }


    public String getFromPortId1() {
        return fromPortId1;
    }

    public void setFromPortId1(String fromPortId) {
        this.fromPortId1 = fromPortId;
    }


    public String getToPortId1() {
        return toPortId1;
    }

    public void setToPortId1(String toPortId) {
        this.toPortId1 = toPortId;
    }

    //------

    public String getFromPortId2() {
        return fromPortId2;
    }

    public void setFromPortId2(String fromPortId) {
        this.fromPortId2 = fromPortId;
    }


    public String getToPortId2() {
        return toPortId2;
    }

    public void setToPortId2(String toPortId) {
        this.toPortId2 = toPortId;
    }
    //-----


    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }


    public String getResourceId1() {
        return resourceId1;
    }

    public void setResourceId1(String resourceId) {
        this.resourceId1 = resourceId;
    }


    public String getFromPortSiteType1() {
        return fromPortSiteType1;
    }

    public void setFromPortSiteType1(String fromPortSiteType) {
        this.fromPortSiteType1 = fromPortSiteType;
    }


    public String getToPortSiteType1() {
        return toPortSiteType1;
    }

    public void setToPortSiteType1(String toPortSiteType) {
        this.toPortSiteType1 = toPortSiteType;
    }

    //-----


    public String getResourceId2() {
        return resourceId2;
    }

    public void setResourceId2(String resourceId) {
        this.resourceId2 = resourceId;
    }


    public String getFromPortSiteType2() {
        return fromPortSiteType2;
    }

    public void setFromPortSiteType2(String fromPortSiteType) {
        this.fromPortSiteType2 = fromPortSiteType;
    }


    public String getToPortSiteType2() {
        return toPortSiteType2;
    }

    public void setToPortSiteType2(String toPortSiteType) {
        this.toPortSiteType2 = toPortSiteType;
    }

    public String getCloudProviderFromPort() {
        return cloudProviderFromPort;
    }

    public void setCloudProviderFromPort(String cloudProviderFromPort) {
        this.cloudProviderFromPort = cloudProviderFromPort;
    }



	@Override
	public String toString() {
		return "CloudConnectionBillResponse [connectionId=" + connectionId
				+ ", name=" + name + ", cloudProviderFromPort="
				+ cloudProviderFromPort + ", fromPortName1=" + fromPortName1
				+ ", toPortName1=" + toPortName1 + ", fromPortName2="
				+ fromPortName2 + ", toPortName2=" + toPortName2
				+ ", aEndAddress1=" + aEndAddress1 + ", bEndAddress1="
				+ bEndAddress1 + ", aEndAddress2=" + aEndAddress2
				+ ", bEndAddress2=" + bEndAddress2 + ", bandwidth=" + bandwidth
				+ ", createdDate=" + createdDate + ", lastUpdated="
				+ lastUpdated + ", decommissionedOn=" + decommissionedOn
				+ ", status=" + status + ", rentalCharge=" + rentalCharge
				+ ", rentalUnit=" + rentalUnit + ", rentalCurrency="
				+ rentalCurrency + ", fromPortId1=" + fromPortId1
				+ ", toPortId1=" + toPortId1 + ", fromPortId2=" + fromPortId2
				+ ", toPortId2=" + toPortId2 + ", serviceId=" + serviceId
				+ ", resourceId1=" + resourceId1 + ", resourceId2="
				+ resourceId2 + ", fromPortSiteType1=" + fromPortSiteType1
				+ ", toPortSiteType1=" + toPortSiteType1
				+ ", fromPortSiteType2=" + fromPortSiteType2
				+ ", toPortSiteType2=" + toPortSiteType2 + "]";
	}
    
}
