package com.colt.novitas.response;

import java.io.Serializable;

import com.colt.novitas.constants.PortStatus;
import com.colt.novitas.constants.RentalUnit;
import com.google.gson.annotations.SerializedName;

public class CloudPortResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3638981178466690380L;

	@SerializedName("id")
	private String id;

	@SerializedName("name")
	private String name;

	@SerializedName("status")
	private PortStatus status;

	@SerializedName("location")
	private String location;

	@SerializedName("used_bandwidth")
	private Integer usedBandwidth;

	@SerializedName("available_bandwidth")
	private Integer availableBandwidth;

	@SerializedName("bandwidth")
	private Integer bandwidth;

	@SerializedName("no_of_connections")
	private Integer noOfConnections;

	@SerializedName("rental_charge")
	private Float rentalCharge;

	@SerializedName("rental_unit")
	private RentalUnit rentalUnit;

	@SerializedName("rental_currency")
	private String rentalCurrency;

	@SerializedName("address")
	private String address;

	@SerializedName("site_type")
	private String siteType;

	@SerializedName("commitment_expiry")
	private String commitmentExpiry;

	@SerializedName("penalty_charge")
	private Float penaltyCharge;

	@SerializedName("service_id")
	private String serviceId;

	@SerializedName("has_p_to_p_mapping")
	private Boolean hasPToPMapping;

	@SerializedName("max_allowed_connections")
	private Integer maxAllowedConnections;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("ocn")
	private String ocn;

	@SerializedName("site_floor")
	private String siteFloor;

	@SerializedName("site_room_name")
	private String siteRoomName;

	@SerializedName("location_premises_number")
	private String locationPremisesNumber;

	@SerializedName("location_building_name")
	private String locationBuildingName;

	@SerializedName("location_street_name")
	private String locationStreetName;

	@SerializedName("location_city")
	private String locationCity;

	@SerializedName("location_state")
	private String locationState;

	@SerializedName("location_country")
	private String locationCountry;

	@SerializedName("postal_zip_code")
	private String postalZipCode;

	@SerializedName("latitude")
	private Float latitude;

	@SerializedName("longitude")
	private Float longitude;

	@SerializedName("location_id")
	private String locationId;

	@SerializedName("cloud_provider")
	private String cloudProvider;

	@SerializedName("azure_service_key")
	private String serviceKey;

	@SerializedName("port")
	private String port;

	@SerializedName("vlan")
	private Integer vlan;

	@SerializedName("resource_id_1")
	private String resourceId1;

	@SerializedName("resource_id_2")
	private String resourceId2;

	@SerializedName("decommissioning_charge")
	private Float decommissioningCharge;

	@SerializedName("decommissioning_currency")
	private String decommissioningCurrency;

	@SerializedName("aws_account_no")
	private String awsAccountno;

	@SerializedName("aws_region")
	private String region;

	@SerializedName("aws_connection_id")
	private String awsConnId;

	@SerializedName("nms")
	private String nms;

	@SerializedName("nni_circuit_id")
	private String nniCircuitId;

	@SerializedName("local_building_name")
	private String localBuildingName;

	@SerializedName("location_city_code")
	private String locationCityCode;
	@SerializedName("location_country_code")
	private String locationCountryCode;
	
	@SerializedName("resource_port_name_1")
	private String resourcePortName1;
    
	@SerializedName("resource_port_name_2")
   	private String resourcePortName2;
	
	@SerializedName("nc_tech_service_id_1")
    private String ncTechServiceId1;
	
	@SerializedName("nc_tech_service_id_2")
    private String ncTechServiceId2;

	public CloudPortResponse() {
		super();
	}

	public String getNms() {
		return nms;
	}

	public void setNms(String nms) {
		this.nms = nms;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PortStatus getStatus() {
		return status;
	}

	public void setStatus(PortStatus status) {
		this.status = status;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getUsedBandwidth() {
		return usedBandwidth;
	}

	public void setUsedBandwidth(Integer usedBandwidth) {
		this.usedBandwidth = usedBandwidth;
	}

	public Integer getAvailableBandwidth() {
		return availableBandwidth;
	}

	public void setAvailableBandwidth(Integer availableBandwidth) {
		this.availableBandwidth = availableBandwidth;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public Integer getNoOfConnections() {
		return noOfConnections;
	}

	public void setNoOfConnections(Integer noOfConnections) {
		this.noOfConnections = noOfConnections;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public RentalUnit getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(RentalUnit rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public String getCommitmentExpiry() {
		return commitmentExpiry;
	}

	public void setCommitmentExpiry(String commitmentExpiry) {
		this.commitmentExpiry = commitmentExpiry;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Boolean getHasPToPMapping() {
		return hasPToPMapping;
	}

	public void setHasPToPMapping(Boolean hasPToPMapping) {
		this.hasPToPMapping = hasPToPMapping;
	}

	public Integer getMaxAllowedConnections() {
		return maxAllowedConnections;
	}

	public void setMaxAllowedConnections(Integer maxAllowedConnections) {
		this.maxAllowedConnections = maxAllowedConnections;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}

	public String getLocationState() {
		return locationState;
	}

	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	public String getLocationStreetName() {
		return locationStreetName;
	}

	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getLocationCountry() {
		return locationCountry;
	}

	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return the cloudProvider
	 */

	public String getCloudProvider() {
		return cloudProvider;
	}

	/**
	 * @param cloudProvider
	 *            the cloudProvider to set
	 */
	public void setCloudProvider(String cloudProvider) {
		this.cloudProvider = cloudProvider;
	}

	/**
	 * @return the serviceKey
	 */

	public String getServiceKey() {
		return serviceKey;
	}

	/**
	 * @param serviceKey
	 *            the serviceKey to set
	 */
	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}

	/**
	 * @return the Port
	 */

	public String getPort() {
		return port;
	}

	/**
	 * @param port
	 *            the Port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * @return the vlan
	 */

	public Integer getVlan() {
		return vlan;
	}

	/**
	 * @param vlan
	 *            the Vlan to set
	 */
	public void setVlan(Integer vlan) {
		this.vlan = vlan;
	}

	/**
	 * @return the resourceId1
	 */

	public String getResourceId1() {
		return resourceId1;
	}

	/**
	 * @param resourceId1
	 *            the resourceId1 to set
	 */
	public void setResourceId1(String resourceId1) {
		this.resourceId1 = resourceId1;
	}

	/**
	 * @return the resourceId2
	 */

	public String getResourceId2() {
		return resourceId2;
	}

	/**
	 * @param resourceId2
	 *            the resourceId2 to set
	 */
	public void setResourceId2(String resourceId2) {
		this.resourceId2 = resourceId2;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public String getAwsAccountno() {
		return awsAccountno;
	}

	public void setAwsAccountno(String awsAccountno) {
		this.awsAccountno = awsAccountno;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getAwsConnId() {
		return awsConnId;
	}

	public void setAwsConnId(String awsConnId) {
		this.awsConnId = awsConnId;
	}

	public String getNniCircuitId() {
		return nniCircuitId;
	}

	public void setNniCircuitId(String nniCircuitId) {
		this.nniCircuitId = nniCircuitId;
	}

	public String getLocalBuildingName() {
		return localBuildingName;
	}

	public void setLocalBuildingName(String localBuildingName) {
		this.localBuildingName = localBuildingName;
	}

	public String getLocationCityCode() {
		return locationCityCode;
	}

	public void setLocationCityCode(String locationCityCode) {
		this.locationCityCode = locationCityCode;
	}

	public String getLocationCountryCode() {
		return locationCountryCode;
	}

	public void setLocationCountryCode(String locationCountryCode) {
		this.locationCountryCode = locationCountryCode;
	}

	public String getResourcePortName1() {
		return resourcePortName1;
	}

	public void setResourcePortName1(String resourcePortName1) {
		this.resourcePortName1 = resourcePortName1;
	}

	public String getResourcePortName2() {
		return resourcePortName2;
	}

	public void setResourcePortName2(String resourcePortName2) {
		this.resourcePortName2 = resourcePortName2;
	}

	public String getNcTechServiceId1() {
		return ncTechServiceId1;
	}

	public void setNcTechServiceId1(String ncTechServiceId1) {
		this.ncTechServiceId1 = ncTechServiceId1;
	}

	public String getNcTechServiceId2() {
		return ncTechServiceId2;
	}

	public void setNcTechServiceId2(String ncTechServiceId2) {
		this.ncTechServiceId2 = ncTechServiceId2;
	}

	@Override
	public String toString() {
		return "CloudPortResponse [id=" + id + ", name=" + name + ", status=" + status + ", location=" + location
				+ ", usedBandwidth=" + usedBandwidth + ", availableBandwidth=" + availableBandwidth + ", bandwidth="
				+ bandwidth + ", noOfConnections=" + noOfConnections + ", rentalCharge=" + rentalCharge
				+ ", rentalUnit=" + rentalUnit + ", rentalCurrency=" + rentalCurrency + ", address=" + address
				+ ", siteType=" + siteType + ", commitmentExpiry=" + commitmentExpiry + ", penaltyCharge="
				+ penaltyCharge + ", serviceId=" + serviceId + ", hasPToPMapping=" + hasPToPMapping
				+ ", maxAllowedConnections=" + maxAllowedConnections + ", customerName=" + customerName + ", ocn=" + ocn
				+ ", siteFloor=" + siteFloor + ", siteRoomName=" + siteRoomName + ", locationPremisesNumber="
				+ locationPremisesNumber + ", locationBuildingName=" + locationBuildingName + ", locationStreetName="
				+ locationStreetName + ", locationCity=" + locationCity + ", locationState=" + locationState
				+ ", locationCountry=" + locationCountry + ", postalZipCode=" + postalZipCode + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", locationId=" + locationId + ", cloudProvider=" + cloudProvider
				+ ", serviceKey=" + serviceKey + ", port=" + port + ", vlan=" + vlan + ", resourceId1=" + resourceId1
				+ ", resourceId2=" + resourceId2 + ", decommissioningCharge=" + decommissioningCharge
				+ ", decommissioningCurrency=" + decommissioningCurrency + ", awsAccountno=" + awsAccountno
				+ ", region=" + region + ", awsConnId=" + awsConnId + ", nms=" + nms + ", nniCircuitId=" + nniCircuitId
				+ ", localBuildingName=" + localBuildingName + ", locationCityCode=" + locationCityCode
				+ ", locationCountryCode=" + locationCountryCode + ", resourcePortName1=" + resourcePortName1
				+ ", resourcePortName2=" + resourcePortName2 + ", ncTechServiceId1=" + ncTechServiceId1
				+ ", ncTechServiceId2=" + ncTechServiceId2 + "]";
	}

	

}