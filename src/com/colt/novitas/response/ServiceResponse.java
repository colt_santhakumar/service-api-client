package com.colt.novitas.response;

import java.io.Serializable;

import com.colt.novitas.constants.NovitasServiceStatus;

public class ServiceResponse implements Serializable {

	private static final long serialVersionUID = 5562102885371202129L;

	private String serviceId;
	private int customerId;
	private NovitasServiceStatus status;

	public ServiceResponse() {
		super();
	}

	public ServiceResponse(String serviceId, int customerId) {
		super();
		this.serviceId = serviceId;
		this.customerId = customerId;
	}

	public ServiceResponse(String serviceId, NovitasServiceStatus status) {
		super();
		this.serviceId = serviceId;
		this.status = status;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public NovitasServiceStatus getStatus() {
		return status;
	}

	public void setStatus(NovitasServiceStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ServiceResponse [serviceId=" + serviceId + ", customerId=" + customerId + ", status=" + status + "]";
	}

}
