package com.colt.novitas.response;

import java.io.Serializable;
import java.util.List;

import com.colt.novitas.constants.ConnectionStatus;
import com.colt.novitas.constants.RentalUnit;
import com.google.gson.annotations.SerializedName;

public class CloudConnectionResponse implements Serializable {

	private static final long serialVersionUID = -1705595914057459510L;

	@SerializedName("connection_id")
	private String connectionId;

	@SerializedName("name")
	private String name;

	@SerializedName("bandwidth")
	private Integer bandwidth;

	@SerializedName("created_date")
	private String createdDate;

	@SerializedName("last_updated")
	private String lastUpdated;

	@SerializedName("decommissioned_on")
	private String decommissionedOn;

	@SerializedName("status")
	private ConnectionStatus status;

	@SerializedName("rental_charge")
	private Float rentalCharge;

	@SerializedName("rental_unit")
	private RentalUnit rentalUnit;

	@SerializedName("rental_currency")
	private String rentalCurrency;

	@SerializedName("service_id")
	private String serviceId;

	@SerializedName("resource_id_1")
	private String resourceId1;

	@SerializedName("resource_id_2")
	private String resourceId2;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("ocn")
	private String ocn;

	@SerializedName("sla_id")
	private String slaId;

	@SerializedName("penalty_charge")
	private Float penaltyCharge;

	@SerializedName("commitment_period")
	private Integer commitmentPeriod;

	@SerializedName("commitment_expiry_date")
	private String commitmentExpiryDate;

	@SerializedName("nms")
	private String nms;

	@SerializedName("component_connections")
	private List<ConnectionPortPair> ports;
	
	@SerializedName("nc_tech_service_id_1")
    private String ncTechServiceId1;
	
	@SerializedName("nc_tech_service_id_2")
    private String ncTechServiceId2;

	@SerializedName("connection_type")
	private String connectionType;

	@SerializedName("base_bandwidth")
	private Integer baseBandwidth;

	@SerializedName("base_rental")
	private Float baseRental;

	public CloudConnectionResponse() {
		super();
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getDecommissionedOn() {
		return decommissionedOn;
	}

	public void setDecommissionedOn(String decommissionedOn) {
		this.decommissionedOn = decommissionedOn;
	}

	public ConnectionStatus getStatus() {
		return status;
	}

	public void setStatus(ConnectionStatus status) {
		this.status = status;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public RentalUnit getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(RentalUnit rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getSlaId() {
		return slaId;
	}

	public void setSlaId(String slaId) {
		this.slaId = slaId;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}

	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}

	/**
	 * @return the resourceId1
	 */
	public String getResourceId1() {
		return resourceId1;
	}

	/**
	 * @param resourceId1
	 *            the resourceId1 to set
	 */
	public void setResourceId1(String resourceId1) {
		this.resourceId1 = resourceId1;
	}

	/**
	 * @return the resourceId2
	 */
	public String getResourceId2() {
		return resourceId2;
	}

	/**
	 * @param resourceId2
	 *            the resourceId2 to set
	 */
	public void setResourceId2(String resourceId2) {
		this.resourceId2 = resourceId2;
	}

	/**
	 * @return the ports
	 */
	public List<ConnectionPortPair> getPorts() {
		return ports;
	}

	/**
	 * @param ports
	 *            the ports to set
	 */
	public void setPorts(List<ConnectionPortPair> ports) {
		this.ports = ports;
	}

	public String getNms() {
		return nms;
	}

	public void setNms(String nms) {
		this.nms = nms;
	}

	public String getNcTechServiceId1() {
		return ncTechServiceId1;
	}

	public void setNcTechServiceId1(String ncTechServiceId1) {
		this.ncTechServiceId1 = ncTechServiceId1;
	}

	public String getNcTechServiceId2() {
		return ncTechServiceId2;
	}

	public void setNcTechServiceId2(String ncTechServiceId2) {
		this.ncTechServiceId2 = ncTechServiceId2;
	}

	public Integer getBaseBandwidth() {
		return baseBandwidth;
	}

	public void setBaseBandwidth(Integer baseBandwidth) {
		this.baseBandwidth = baseBandwidth;
	}

	public Float getBaseRental() {
		return baseRental;
	}

	public void setBaseRental(Float baseRental) {
		this.baseRental = baseRental;
	}

	public String getConnectionType() {
		return connectionType;
	}

	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}

	@Override
	public String toString() {
		return "CloudConnectionResponse{" +
				"connectionId='" + connectionId + '\'' +
				", name='" + name + '\'' +
				", bandwidth=" + bandwidth +
				", createdDate='" + createdDate + '\'' +
				", lastUpdated='" + lastUpdated + '\'' +
				", decommissionedOn='" + decommissionedOn + '\'' +
				", status=" + status +
				", rentalCharge=" + rentalCharge +
				", rentalUnit=" + rentalUnit +
				", rentalCurrency='" + rentalCurrency + '\'' +
				", serviceId='" + serviceId + '\'' +
				", resourceId1='" + resourceId1 + '\'' +
				", resourceId2='" + resourceId2 + '\'' +
				", customerName='" + customerName + '\'' +
				", ocn='" + ocn + '\'' +
				", slaId='" + slaId + '\'' +
				", penaltyCharge=" + penaltyCharge +
				", commitmentPeriod=" + commitmentPeriod +
				", commitmentExpiryDate='" + commitmentExpiryDate + '\'' +
				", nms='" + nms + '\'' +
				", ports=" + ports +
				", ncTechServiceId1='" + ncTechServiceId1 + '\'' +
				", ncTechServiceId2='" + ncTechServiceId2 + '\'' +
				", connectionType='" + connectionType + '\'' +
				", baseBandwidth=" + baseBandwidth +
				", baseRental=" + baseRental +
				'}';
	}


}
