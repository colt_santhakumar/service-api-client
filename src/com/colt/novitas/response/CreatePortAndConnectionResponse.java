package com.colt.novitas.response;

public class CreatePortAndConnectionResponse {
	
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CreatePortAndConnectionResponse() {
		super();
	}
}
